# Thingsat :: public repository

[ThingSat project](https://www.csug.fr/projects/thingsat-project/) developed by [CSUG](https://www.csug.fr/en/) and its [partners](#Partners) aims to create a CubeSat payload dedicated to design and to benchmark long-distance communication protocols with low-power consumption LoRa modulation.

## A propos de ThingSat (in french)

* [Video Partie 1 : Le satellite](https://youtu.be/aUAwzN8-94M?si=LgHdwP1PvHfyvpui)
* [Video Partie 2 : La mission](https://youtu.be/cGfpBNYOTBU?si=XpqAYzhQyqtXe5Tm)
* [Video Partie 3: Le déploiement](https://youtu.be/WMljNrJ-3Kw?si=hGc93_4ZKevs1RTO)
* [Video Partie 4: Un travail d'équipe](https://youtu.be/uPJovwitR38?si=AKi3j0m0AtdsSJMq)
* [Video :  Lacher des ballons jusqu'aux portes de l'Espace !](https://youtu.be/pSnj2P_UTQc?si=bPKkHkdpBr3O0Z7e)

## Projects

* [High altitude LoRa Benchmarks from sounding balloons](./balloons)
* [LoRa benchmarks from cubesats](./cubesat_mission)
* [Thingsat's gateways](./gateways)
* [Thingsat's Command and Control Center (CCC)](./ccc)

## Presentations & Papers
* [References](cubesat_mission/publications.md)

## About Thingsat
* https://www.csug.fr/projects/thingsat-project/

## About CSUG
* https://www.csug.fr/main-menu/csug/
