# Thingsat :: 📡📡📡📡📡 High altitude LoRa Benchmarking 🎈🎈🎈🎈🎈🎈 🛰️🛰️🛰️🛰️🛰️🛰️

## Aims
* Benchmarking LoRa links over very long distance (> 500 kms) for several frequency bands (433MHz, 868MHz, 2.4GHz).
* Studying frames of ground LoRaWAN endpoints from the near-space.
* Repeating LoRa frames of ground LoRaWAN trackers (emergency usecase).

## Sounding balloons flights

The geometric horizon is 618.27 km and the radar horizon is 713.92 km at an altitude of 30,000 meters.

High-altitude LoRa communication benchmarks had been carried out using sounding balloons. 

* [Valence 🏁, May 2019](./2019-05-09)
* [Aire-sur-l'Adour 🏁, September 2020](./2020-09-23)
* [Aire-sur-l'Adour 🏁, April 2021](./2021-04-15)
* [Aire-sur-l'Adour 🏁, June 9th 2021](./2021-06-09)
* [Roman-sur-Isère 🏁, June 15th 2021](./2021-06-15)
* [Aire-sur-l'Adour 🏁, October 31st 2023](./2023-10-31)
* [Aire-sur-l'Adour 🏁, May 24th 2024](./2024-05-24)
* [Grenoble 🏁, November 2nd 2024](./2024-11-02)
* [Aire-sur-l'Adour 🏁, Spring 2025](./2025-05)

## Datasets

Soon on [PerSCiDO](https://perscido.univ-grenoble-alpes.fr/).

![gondola_preparation](./gondola_preparation-03.jpg)
![gondola_ready_to_fly](./2024-05-24/media/gondola_ready_to_fly.jpg)
![multi-frequencies-rx-station](2024-05-24/media/multi-frequencies-rx-station-01.jpg)
![inflating](2024-05-24/media/balloon_inflating-03.jpg)
![Launching](./2021-04-15/media/launching-1.jpg)
![Burst](./2024-05-24/media/burst-02.jpg)



