/*
 Thingsat project

 Copyright (c) 2021-2024 UGA CSUG LIG

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */


/*
 * Thingsat Mission Scenarii :: fpayload definition for RANGING mission scenario
 */

/*
 * Author : Didier Donsez, Université Grenoble Alpes
 */


#ifndef THINGSAT_RANGING_H
#define THINGSAT_RANGING_H

#include <stdint.h>
#include <inttypes.h>

#define THINGSAT_RANGING_ENDPOINTS_LEN		4


#ifndef REPEATER_RANGING_MSG_1_FPORT
#define REPEATER_RANGING_MSG_1_FPORT	(161U)
#endif

#ifndef REPEATER_RANGING_MSG_2_FPORT
#define REPEATER_RANGING_MSG_2_FPORT	(162U)
#endif

#ifndef REPEATER_RANGING_MSG_3_FPORT
#define REPEATER_RANGING_MSG_3_FPORT	(163U)
#endif

// TODO adapt value according SF ...
#ifndef REPEATER_COUNTER_DELTA_FOR_TX
// 500000 usec
#define REPEATER_COUNTER_DELTA_FOR_TX		(500000U)
#endif


/**
 * Struct for Ranging location
 */
struct __attribute__((__packed__)) ranging_location {

	// TODO add GPS Fix last time (in seconds), GPS quality, GPS satellite number ...

	/**
	 * @brief Latitude	(in degree)
	 * TODO optimization on int24 with pack_coord(latitude,longitude)
	 */
	float latitude;

	/**
	 * @brief Longitude (in degree)
	 * TODO optimization on int24 with pack_coord()
	 */
	float longitude;

	/**
	 * @brief Altitude (in meter)
	 */
	uint16_t altitude;

};

typedef struct ranging_location ranging_location_t;

/**
 * Struct for Ranging message for step #1
 * Initiating the TWR protocol
 */
struct __attribute__((__packed__)) ranging_msg_1 {

	/**
	 * @brief Location
	 * optional field but useful for checking distance during the balloon flight
	 */
	ranging_location_t location;
};

typedef struct ranging_msg_1 ranging_msg_1_t;

/**
 * Struct for Ranging message for step #2 for reply to step #1
 */
struct __attribute__((__packed__)) ranging_msg_2 {

	/**
	 * @brief DevAddr of sender of the message #1
	 * can be optimize as uint8_t for the index into the SWARM_DEVADDR array since SWARM_SIZE < 256.
	 */
	uint32_t devaddr;

	/**
	 * @brief the two least significant bytes of 32 bits fCnt of the message #1
	 */
	uint16_t fCnt;

	/**
	 * @brief Delay (seconds part) for replying to the message #1
	 * The delay must not exceed 255 seconds
	 */
	uint8_t delay_sec_before_replying;

	/**
	 * @brief Delay (nanoseconds part) for replying to the message #1
	 */
	uint32_t delay_nsec_before_replying;

	/**
	 * @brief Location
	 * optional field but useful for checking distance during the balloon flight
	 */
	ranging_location_t location;
};

typedef struct ranging_msg_2 ranging_msg_2_t;

/**
 * Struct for Ranging message for step #3 for reply to step #2
 */
struct __attribute__((__packed__)) ranging_msg_3 {

	/**
	 * @brief DevAddr of sender of the message #2
	 * can be optimize as uint8_t for the index into the SWARM_DEVADDR array since SWARM_SIZE < 256.
	 */
	uint32_t devaddr;

	/**
	 * @brief the two least significant bytes of 32 bits fCnt of the message #2
	 */
	uint16_t fCnt;

	/**
	 * @brief Delay (seconds part) for replying to the message #2
	 * The delay must not exceed 255 seconds
	 */
	uint8_t delay_sec_before_replying;

	/**
	 * @brief Delay (nanoseconds part) for replying to message #2
	 */
	uint32_t delay_nsec_before_replying;

	/**
	 * @brief Location
	 * optional field but useful for checking distance during the balloon flight
	 */
	ranging_location_t location;

	/**
	 * @brief Distance between the sender and the receiver identified with the devaddr field (in meter)
	 * optional field but useful for checking distance during the balloon flight
	 */
	uint32_t distance;

	/**
	 * @brief Distances between the sender and the others receivers in the swarm
	 * 0 means unknown distance
	 * optional field but useful for checking distance during the balloon flight
	 */
	uint32_t distances[THINGSAT_RANGING_ENDPOINTS_LEN-2];
};

typedef struct ranging_msg_3 ranging_msg_3_t;

bool ranging_is_endpoint_frame(const struct lgw_pkt_rx_s *rxpkt, int *endpoint_idx);

size_t ranging_fill_msg_1(ranging_msg_1_t *msg);

int8_t ranging_process_msg_1(const uint32_t devaddr, const uint16_t fCnt, const uint8_t *payload, const size_t size);

int8_t ranging_process_msg_2(const uint32_t devaddr, const uint16_t fCnt, const uint8_t *payload, const size_t size);

int8_t ranging_process_msg_3(const uint32_t devaddr, const uint16_t fCnt, const uint8_t *payload, const size_t size);

void ranging_print_location(const ranging_location_t *location);

void ranging_print_msg_1(const ranging_msg_1_t *msg);

void ranging_print_msg_2(const ranging_msg_2_t *msg);

void ranging_print_msg_3(const ranging_msg_3_t *msg);

#endif
