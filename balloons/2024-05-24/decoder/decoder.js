/*
 Thingsat project

 Payload decoder
 Copyright (c) 2021-2024 UGA CSUG LIG

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */
 
/*
 * Author: Didier Donsez, Université Grenoble Alpes
 */

/*
 * Decode the Thingsat frame payload
 */


// Javascript functions for décoding and encoding the frame payloads
// From https://github.com/feross/buffer/blob/master/index.js

function readUInt16LE(buf, offset) {
	offset = offset >>> 0;
	return buf[offset] | (buf[offset + 1] << 8);
}

function readInt16LE(buf, offset) {
	offset = offset >>> 0;
	var val = buf[offset] | (buf[offset + 1] << 8);
	return (val & 0x8000) ? val | 0xFFFF0000 : val;
}

function readUInt24LE(buf, offset) {
	offset = offset >>> 0;

	return ((buf[offset]) |
		(buf[offset + 1] << 8)) +
		(buf[offset + 2] * 0x10000);
}

function readUInt32LE(buf, offset) {
	offset = offset >>> 0;

	return ((buf[offset]) |
		(buf[offset + 1] << 8) |
		(buf[offset + 2] << 16)) +
		(buf[offset + 3] * 0x1000000);
}

function readIntLE(buf, offset, byteLength) {
	offset = offset >>> 0;
	byteLength = byteLength >>> 0;

	var val = buf[offset];
	var mul = 1;
	var i = 0;
	while (++i < byteLength && (mul *= 0x100)) {
		val += buf[offset + i] * mul;
	}
	mul *= 0x80;

	if (val >= mul) val -= Math.pow(2, 8 * byteLength);

	return val;
}

function readInt32LE(buf, offset) {
	return readIntLE(buf, offset, 4);
}

function readInt8(buf, offset) {
	offset = offset >>> 0;
	if (!(buf[offset] & 0x80)) return (buf[offset]);
	return ((0xff - buf[offset] + 1) * -1);
}

function readUInt8(buf, offset) {
	offset = offset >>> 0;
	return (buf[offset]);
}

function readInt8(buf, offset) {
	offset = offset >>> 0;
	if (!(buf[offset] & 0x80)) return (buf[offset]);
	return ((0xff - buf[offset] + 1) * -1);
}
/*
*  Usage: decodeFloat32(readUInt32LE (buf, offset))
*/
function decodeFloat32(uint32) {

	// since JSON does not support NaN
	if (uint32 === 0x7FC00000) return 'NaN';

	var sign = (uint32 & 0x80000000) ? -1 : 1;
	var exponent = ((uint32 >> 23) & 0xFF) - 127;
	var significand = (uint32 & ~(-1 << 23));

	if (exponent == 128)
		// since JSON does not support NaN
		return sign * ((significand) ? 'NaN' : Number.POSITIVE_INFINITY);
	//return sign * ((significand) ? Number.NaN : Number.POSITIVE_INFINITY);

	if (exponent == -127) {
		if (significand == 0) return sign * 0.0;
		exponent = -126;
		significand /= (1 << 22);
	} else significand = (significand | (1 << 23)) / (1 << 23);

	var r = sign * significand * Math.pow(2, exponent);

	return r;
}

// https://github.com/CampusIoT/payload-codec/blob/master/src/main/javascript/semtech/semtech_loramote_codec.js

// Value used for the conversion of the position from DMS to decimal
/*
const MaxNorthPosition = 8388607; // 2^23 - 1
const MaxSouthPosition = 8388608; // -2^23
const MaxEastPosition = 8388607; // 2^23 - 1
const MaxWestPosition = 8388608; // -2^23
*/

/*
In LoRaWAN Spec 1.0 (page 75/80)
16.3.1 Gateway GPS coordinate:1 InfoDesc = 0, 1 or 2
2 For InfoDesc = 0 ,1 or 2, the content of the Info field encodes the GPS coordinates of the
3 antenna broadcasting the beacon
Size (bytes) 3 3
Info Lat Lng
4 The latitude and longitude fields (Lat and Lng, respectively) encode the geographical
5 location of the gateway as follows:
6 • The north-­south latitude is encoded using a signed 24 bit word where -­223
7 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the
8 North Pole). The equator corresponds to 0.
9 • The east-­west longitude is encoded using a signed 24 bit word where -­
10 223corresponds to 180° west and 223 corresponds to 180° east. The Greenwich
11 meridian corresponds to 0.
*/

/*
// Utility functions
Number.prototype.roundUsing = function (func, prec) {
	var temp = this * Math.pow(10, prec);
	temp = func(temp);
	return temp / Math.pow(10, prec);
}
*/

/*
function getDegree(payload, pos, maxDeg) {
	// payload is a baffer containing 3 bytes (signed 24b integer) at position pos
	// maxDeg is 90 for latitude and 180 for longitude

	var val = payload[pos+0]<<16 + payload[pos+1]<<8 + payload[pos+2]; // BE or LE
	res = (val * maxDeg / MaxEastPosition).roundUsing(Math.ceil, 5);

	return res;
}
*/

/* TODO function decodeLatitude24(payload, offset) {
		// TODO undefined latitude
		 // TODO should fix for negative latitude
		 // IN THIS VERSION,
		var _latitude = payload.readUInt32BE(offset);
		//console.log("_latitude:",_latitude);
		//_latitude = _latitude & 0x00FFFFFF;
		_latitude = _latitude >> 8;
		//console.log("_latitude:",_latitude);
		_latitude = (_latitude * 90.0 / MaxNorthPosition).roundUsing(Math.ceil, 5);
		//console.log("_latitude:",_latitude);
		value["latitude"] = _latitude;

}
*/

/* TODO function decodeLongitude24(payload, offset) {
		// TODO undefined longitude
		// TODO should fix for negative longitude
		var _longitude = payload.readUInt32BE(offset);
		//console.log("_longitude:",_longitude);
		//_longitude = _longitude & 0x00FFFFFF;
		_longitude = _longitude >> 8;
		//console.log("_longitude:",_longitude);
		_longitude = (_longitude * 180.0 / MaxEastPosition).roundUsing(Math.ceil, 5);
		//console.log("_longitude:",_longitude);
		value["longitude"] = _longitude;
}
*/

// ===================================================
// Decode payloads
// ---------------------------------------------------


// See lorawan_payload.h
function getPayloadType(fPort) {

	switch (fPort) {
		case 0:
			return 'mac';
		case 1:
			return 'sos';
		case 2:
			return 'diag';
		case 3:
			return 'repeated';
		case 4:
			return 'telemetry';
		case 9:
			return 'tle';
		case 10:
			return 'dn_text';
		case 11:
			return 'ed25519_pubkey';
		case 202:
			return 'app_clock';
		case 33:
			return 'up_text';
		case 34:
			return 'forwarding';
		case 35:
			return 'action';
		case 36:
			return 'timing_test';
		case 100:
			return 'repeater_frame';
		case 110:
			return 'repeater_stat';
		case 120:
			return 'repeater_nav';
		case 130:
			return 'repeater_temp';
		case 161:
			return 'repeater_ranging_msg_1';
		case 162:
			return 'repeater_ranging_msg_2';			
		case 163:
			return 'repeater_ranging_msg_3';			
		default:
			if (fPort >= 64 && fPort <= (64 + 32)) {
				return 'xor' + (fPort - 64);
			} else {
				return 'unknown';
			}
	}
}

function decodeSos(fPort, bytes) {
	var len = bytes.length;

	var res = {};
	var idx = 0;

	if (idx + 4 > len) { return res; }
	res.firmware_version = readUInt32LE(bytes, idx);
	idx += 4;

	if (idx + 1 > len) { return res; }
	res.slot_id = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 3 > len) { return res; }
	res.uptime = readUInt24LE(bytes, idx);
	idx += 3;

	if (idx + 1 > len) { return res; }
	res.error_status = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.temperature = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 4 > len) { return res; }
	res.roll = readInt32LE(bytes, idx);
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.pitch = readInt32LE(bytes, idx);
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.yaw = readInt32LE(bytes, idx);
	idx += 4;

	if (idx + 1 > len) { return res; }
	res.last_action_endpoint_index = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 2 > len) { return res; }
	res.last_action_fcnt = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 2 > len) { return res; }
	res.tx_cnt = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 2 > len) { return res; }
	res.tx_abort_cnt = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 2 > len) { return res; }
	res.rx_ok_cnt = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 2 > len) { return res; }
	res.rx_no_crc_cnt = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 2 > len) { return res; }
	res.rx_bad_crc_cnt = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 2 > len) { return res; }
	res.rx_endpoint_cnt = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 2 > len) { return res; }
	res.rx_jreq_cnt = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 2 > len) { return res; }
	res.rx_network_cnt = readUInt16LE(bytes, idx);
	idx += 2;

	return res;
}

function decodeDiag(fPort, bytes) {

	var len = bytes.length;

	var res = {};
	var idx = 0;

	if (idx + 4 > len) { return res; }
	res.firmware_version = readUInt32LE(bytes, idx);
	idx += 4;

	if (idx + 1 > len) { return res; }
	res.slot_id = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 3 > len) { return res; }
	res.uptime = readUInt24LE(bytes, idx);
	idx += 3;

	if (idx + 1 > len) { return res; }
	res.temperature = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 4 > len) { return res; }
	res.timestamp = readUInt32LE(bytes, idx);
	idx += 4;

	if (idx + 2 > len) { return res; }
	res.seconds_before_poweroff = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 4 > len) { return res; }
	res.latitude = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.longitude = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.altitude = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.x = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.y = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.z = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.w = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	return res;
}

function decodeRepeated(fPort, bytes) {
	// TODO SHOULD BE TESTED

	var len = bytes.length;

	var res = {};
	var idx = 0;

	if (idx + 2 > len) { return res; }
	res.exp_id = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 1 > len) { return res; }
	res.entry_id = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.dr = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.esp = readUInt8(bytes, idx) * -1;
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.rfpower = readUInt8(bytes, idx);
	idx += 1;

	// TODO uint8_t frame[REPEATED_FRAME_MAX_LEN];

	return res;
}

function decodeTelemetry(fPort, bytes) {
	var len = bytes.length;

	var res = {};
	var idx = 0;

	if (idx + 2 > len) { return res; }
	res.entry_id = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 1 > len) { return res; }
	res.entry_id = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.rfpower = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 4 > len) { return res; }
	res.timestamp = readUInt32LE(bytes, idx);
	idx += 4;

	if (idx + 2 > len) { return res; }
	res.seconds_before_poweroff = readUInt16LE(bytes, idx);
	idx += 2;

	if (idx + 1 > len) { return res; }
	res.temperature = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 4 > len) { return res; }
	res.latitude = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.longitude = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.altitude = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.x = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.y = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.z = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.w = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.roll = readInt32LE(bytes, idx);
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.pitch = readInt32LE(bytes, idx);
	idx += 4;

	if (idx + 4 > len) { return res; }
	res.yaw = readInt32LE(bytes, idx);
	idx += 4;

	return res;
}

function decodeTle(fPort, bytes) {
	// TODO SHOULD BE TESTED

	var len = bytes.length;

	var res = {};
	var idx = 0;

	if (idx + 1 > len) { return res; }
	res.time_param_prec = readUInt8(bytes, idx) & 0x07;
	idx += 1;

	if (idx + 4 > len) { return res; }
	res.timestamp = readUInt32LE(bytes, idx);
	idx += 4;

	if (idx + 3 > len) { return res; }
	res.noradnumber = readUInt24LE(bytes, idx);
	idx += 3;

	// TO BE CONTINUED

	return res;
}

function decodeDnText(fPort, bytes) {
	return {};
}

function decodeEd25519PubKey(fPort, bytes) {
	return {};
}

function decodeAppClock(fPort, bytes) {
	// TODO SHOULD BE TESTED

	var len = bytes.length;

	var res = {};
	var idx = 0;

	if (idx + 1 > len) { return res; }
	res.cid0 = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.package_identifier = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.package_version = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.cid1 = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 1 > len) { return res; }
	res.param_prec = readUInt8(bytes, idx) & 0x03; // 0b00000011
	idx += 1;

	if (idx + 4 > len) { return res; }
	res.time_to_set = readUInt32LE(bytes, idx);
	idx += 4;

	if (idx + 1 > len) { return res; }
	res.cid2 = readUInt8(bytes, idx);
	idx += 1;

	if (idx + 3 > len) { return res; }
	res.noradnumber = readUInt24LE(bytes, idx);
	idx += 3;

	if (idx + 3 > len) { return res; }
	res.lat24 = readUInt24LE(bytes, idx); // Should be converted
	idx += 3;

	if (idx + 3 > len) { return res; }
	res.lon24 = readUInt24LE(bytes, idx);  // Should be converted
	idx += 3;

	if (idx + 3 > len) { return res; }
	res.alt24 = readUInt24LE(bytes, idx);  // Should be converted
	idx += 3;

	if (idx + 1 > len) { return res; }
	var loc_param = readUInt8(bytes, idx);
	res.loc_param_source = loc_param >> 7;
	res.loc_param_prec = loc_param & 0x7F; //0b01111111
	idx += 1;

	return res;
}

function decodeUpText(fPort, bytes) {
	return {};
}

function decodeForwarding(fPort, bytes) {
	return {};
}

function decodeAction(fPort, bytes) {
	return {};
}

function decodeTimingTest(fPort, bytes) {
	return {};
}

function decodeRepeaterFrame(fPort, bytes) {
	var len = bytes.length;
	return {
		frame: bytes.subarray(0, len - 4).toString('hex'),
		fcnt: readUInt8(bytes, len - 4),
		rssi: - readInt8(bytes, len - 3),
		snr: readInt8(bytes, len - 2),
		dr: readUInt8(bytes, len - 1)
	};
}

function decodeRepeaterStat(fPort, bytes) {
	// TODO protobuf https://github.com/protobufjs/protobuf.js/
	return {
		protobuf: bytes.toString('hex')
	};
}

function decodeRepeaterNavigation(fPort, bytes) {

	var len = bytes.length;

	if(len == 1) {
		return {
			uptime: readUInt32LE(bytes, 0)
		};
	} else if(len == 24) {
		return {
			uptime: readUInt32LE(bytes, 0),
			gps_time: readUInt32LE(bytes, 4),
			latitude: decodeFloat32(readUInt32LE(bytes, 8)),
			longitude: decodeFloat32(readUInt32LE(bytes, 12)),
			altitude: readUInt16LE(bytes, 16),
			speed: readUInt16LE(bytes, 18),
			track: readInt16LE(bytes, 20),
			satellites: readUInt8(bytes, 22),
			quality: readUInt8(bytes, 23)
		};
	} else {
		return {
			error: 'bad_payload_size'
		};
	}
}

function decodeRepeaterTemperatures(fPort, bytes) {

	var len = bytes.length;

	if(len == 12 /* 4+4*2 */) {
		return {
			uptime: readUInt32LE(bytes, 0),
			temperature: readInt16LE(bytes, 4)/100.0,
			temperature_1: readInt16LE(bytes, 6)/100.0,
			temperature_2: readInt16LE(bytes, 8)/100.0,
			temperature_3: readInt16LE(bytes, 10)/100.0			
		};
	} else {
		return {
			error: 'bad_payload_size'
		};
	}
}


function decodeRepeaterRangingLocation(fPort, bytes, res, idx) {

	var len = bytes.length;
	
	if (idx + (4*2) > len) { return res; }
	res.latitude = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;
	res.latitude = decodeFloat32(readUInt32LE(bytes, idx));
	idx += 4;
	if (idx + 2 > len) { return res; }
	res.altitude = readUInt16LE(bytes, idx);
	idx += 2;
	
	return res;
}


function decodeRepeaterRangingMsg1(fPort, bytes) {

	var len = bytes.length;
	
	var res = {};
	var idx = 0;

	res.type = 1;

	if (idx + (4*2 + 2) > len) {
		res = decodeRepeaterRangingLocation(fPort, bytes, res, idx);
	}

	return res;
}

function decodeRepeaterRangingMsg2(fPort, bytes) {

	var len = bytes.length;

	var res = {};
	var idx = 0;

	res.type = 2;
	
	if (idx + 4 > len) { return res; }
	res.devaddr = readUInt32LE(bytes, idx);
	idx += 4;

	if (idx + 2 > len) { return res; }
	res.fCnt = readUInt16LE(bytes, idx);
	idx += 2;
	
	if (idx + 1 > len) { return res; }
	res.delay_sec_before_replying = readUInt8(bytes, idx);
	idx += 1;
	
	if (idx + 4 > len) { return res; }
	res.delay_nsec_before_replying = readUInt32LE(bytes, idx);
	idx += 4;
	
	if (idx + (4*2 + 2) > len) {
		res = decodeRepeaterRangingLocation(fPort, bytes, res, idx);
		idx += (4*2 + 2);
	}
	
	return res;
}

function decodeRepeaterRangingMsg2(fPort, bytes) {

	var len = bytes.length;

	var res = {};
	var idx = 0;
	
	res.type = 3;
	
	if (idx + 4 > len) { return res; }
	res.devaddr = readUInt32LE(bytes, idx);
	idx += 4;

	if (idx + 2 > len) { return res; }
	res.fCnt = readUInt16LE(bytes, idx);
	idx += 2;
	
	if (idx + 1 > len) { return res; }
	res.delay_sec_before_replying = readUInt8(bytes, idx);
	idx += 1;
	
	if (idx + 4 > len) { return res; }
	res.delay_nsec_before_replying = readUInt32LE(bytes, idx);
	idx += 4;
	
	if (idx + (4*2 + 2) > len) {
		res = decodeRepeaterRangingLocation(fPort, bytes, res, idx);
		idx += (4*2 + 2);
	}

	if (idx + 4 > len) { return res; }
	res.distance = readUInt32LE(bytes, idx);
	idx += 4;
	
	var distances = [];
	for(var i=0; idx + 4 > len; i++) {
		distances.push(readUInt32LE(bytes, idx));
		idx += 4;
	}
	if(i>0) {
		res.distances = distances;
	}
	
	return res;
}

function decodeXor(fPort, bytes) {
	return { ratio: fPort - 64, buf: bytes };
}

// See lorawan_payload.h
function decodePayload(fPort, bytes) {

	switch (fPort) {
		case 1:
			return decodeSos(fPort, bytes);
		case 2:
			return decodeDiag(fPort, bytes);
		case 3:
			return decodeRepeated(fPort, bytes);
		case 4:
			return decodeTelemetry(fPort, bytes);
		case 9:
			return decodeTle(fPort, bytes);
		case 10:
			return decodeDnText(fPort, bytes);
		case 11:
			return decodeEd25519PubKey(fPort, bytes);
		case 202:
			return decodeAppClock(fPort, bytes);
		case 33:
			return decodeUpText(fPort, bytes);
		case 34:
			return decodeForwarding(fPort, bytes);
		case 35:
			return decodeAction(fPort, bytes);
		case 100:
			return decodeRepeaterFrame(fPort, bytes);
		case 110:
			return decodeRepeaterStat(fPort, bytes);
		case 120:
			return decodeRepeaterNavigation(fPort, bytes);
		case 130:
			return decodeRepeaterTemperatures(fPort, bytes);
			
		case 161:
			return decodeRepeaterRangingMsg1(fPort, bytes);
		case 162:
			return decodeRepeaterRangingMsg2(fPort, bytes);
		case 163:
			return decodeRepeaterRangingMsg3(fPort, bytes);
			
		default:
			if (fPort >= 64 && fPort <= (64 + 32)) {
				return decodeXor(fPort, bytes);
			} else {
				return {};
			}
	}
}

function decodeUp(bytes, fPort) {

	var decoded = decodePayload(fPort, bytes);
	var payload_type = getPayloadType(fPort);
	decoded.port = fPort;
	decoded.payload_type = payload_type;

	return decoded;
}


// ===================================================
// LNS adapters
// ---------------------------------------------------

/*
* Chirpstack : Decode decodes an array of bytes into an object.
*/
function Decode(fPort, bytes, variables) {
	var decoded = decodeUp(bytes, fPort);
	//decoded._variables = variables;
	return decoded;
}

/*
* Helium : decode decodes an array of bytes into an object.
*/
function Decoder(bytes, port, uplink_info) {

	var decoded = decodeUp(bytes, port);

	/*
	  The uplink_info variable is an OPTIONAL third parameter that provides the following:
	
	  uplink_info = {
		type: "join",
		uuid: <UUIDv4>,
		id: <device id>,
		name: <device name>,
		dev_eui: <dev_eui>,
		app_eui: <app_eui>,
		metadata: {...},
		fcnt: <integer>,
		reported_at: <timestamp>,
		port: <integer>,
		devaddr: <devaddr>,
		hotspots: {...},
		hold_time: <integer>
	  }
	*/

	if (uplink_info) {
		// do something with uplink_info fields
		decoded._uplink_info = uplink_info;
	}

	// for Mapper and Cargo https://docs.helium.com/use-the-network/console/integrations/cargo/
	if (decoded.latitude && decoded.longitude && decoded.altitude) {
		decoded.payload = {
			latitude: decoded.latitude,
			longitude: decoded.longitude,
			altitude: decoded.altitude * 1000, // in meter (not in kilometer)
			speed: 16986.84 // in mph (from https://isstracker.pl/en/satelity/51087)
			//battery	Battery Voltage
		};
	}

	return decoded;
}

/*
* SHOULD BE TESTED
* TTN v3 : decode decodes an array of bytes into an object.
* See https://www.thethingsindustries.com/docs/integrations/payload-formatters/create/
* See https://www.thethingsindustries.com/docs/integrations/payload-formatters/javascript/uplink-decoder/
*/
function decodeUplink(input) {
	return {
		data: decodeUp(input.bytes, input.fPort),
		warnings: [],
		errors: []
	};
}


var p = msg.payload;
if(p.value && p.value.payload) {
    var bytes = Buffer.from(p.value.payload,'hex');
    var fPort = p.metadata.network.lora.port;
    msg.payload.decoded = Decode(fPort, bytes, null);
}

return msg;
