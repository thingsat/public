# Thingsat :: High altitude LPWAN/LPGAN Benchmarking :: May 24th, 2024

## Aims

* Testing the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space conditions (vaccum and -60°C).
* Testing the [REPEAT mission scenario](../../cubesat_mission_2/mission_scenario/README.md#repeat868-mission-scenario) of the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space.
* Testing the [Two-way ranging (with fine timestamping)](../../cubesat_mission_2/mission_scenario#mission-scenario) of the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space.
* Testing fragmented file transfert from ground to balloon and from balloon to ground using [XOR frame redundancy](../../cubesat_mission_2/messages/lorawan_payload.h?ref_type=heads#L48).
* [Collecting statistics on frames sent by ground LoRaWAN endpoints](../../cubesat_mission_2/cubesat_mission_2/mission_scenario/README.md#rx868-mission-scenario) from the near-space.
* Benchmarking LR-FHSS links over very long distance (> 500 kms) for EU868 bands (868 MHz). With CITI Lab
* Benchmarking Narrow Band LoRa links over very long distance (> 500 kms) for EU868 bands (433, 868, 2400 MHz).
* Benchmarking LoRa links over very long distance (> 500 kms) for EU868 bands (868MHz).
* Benchmarking cellular IoT links (NB-IoT) at high-altitude (up to 30 kms).
* Benchmarking SatIoT communications for high-altitude balloon tracking
* Testing LoRaWAN roaming for high-altitude balloon. With Actility, Requea and Helium.
* Testing LoRaWAN [CORECONF](https://datatracker.ietf.org/doc/draft-ietf-core-comi/) frames for high-altitude balloon. With IMT Atlantique.


## Balloon gondolas

The weight of the balloon basket must weigh less than 2800 grams. The material is expanded polystyrene.

Endpoints and gateway include sensors such as temperature, humidity, atmospheric pressure, accelerometer, magnetometer and gyroscope.

Some GNSS modules are configured in balloon mode.

CNES Radio sondes send [APRS packets](https://en.wikipedia.org/wiki/Automatic_Packet_Reporting_System) at 404.002 MHz

4K sportcams are on-board for shooting the flight.

### Balloon gondola #1

* CNES [Radio sonde M20](https://www.meteomodem.com/m20?lang=fr) 
* CNES IRMA GPS tracker (Iridium) 
* Nucleo F446RE + RAK5146 (endpoint+repeater),  LoRaWAN eu868, Orange network (ABP)
* Semtech LoRaMote, LoRaWAN eu868, Orange network (OTAA)
* [Semtech LR1120DVK1TCKS](https://www.semtech.fr/products/wireless-rf/lora-edge/lr1120dvk1tcks): Nucleo L476RG + LR1120 + Active GNSS antenna, [LoRaWAN eu868 (LR-FHSS)](https://github.com/Lora-net/SWSD003/tree/master/lr11xx/apps/lrfhss), TTN network (OTAA) 
* [Semtech LR1110TRK1BKS](https://www.semtech.fr/products/wireless-rf/lora-edge/lr1110trk1bks), LoRaWAN eu868, TTN (OTAA)
* [IMST iM880a + coreconf](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, TTN network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, TTN network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, Orange network (ABP)
* [Wyres Base](https://github.com/CampusIoT/RIOT-wyres), LoRaWAN eu868, Requea+Helium (ABP)
* [Wyres Base](https://github.com/CampusIoT/RIOT-wyres), LoRaWAN eu868, Requea+Helium (ABP)
* ANS Innovation Geditech LoRa industrial endpoint (Narrow Band LoRa)
* ANS Innovation LoRa 2.4 GHz endpoint
* ANS Innovation STM32WL55 433 MHz endpoint 
* ANS Innovation STM32WL55 868 MHz endpoint
* ANS Innovation STM32WL55 868 MHz endpoint (Narrow Band LoRa on 869.525 MHz)

### Balloon gondola #2

* CNES [Radio sonde M20](https://www.meteomodem.com/m20?lang=fr) 
* CNES IRMA GPS tracker (Iridium) 
* Nucleo F446RE  + RAK5146 (endpoint+repeater),  LoRaWAN eu868, Requea network +Helium roaming (ABP)
* Semtech LoRaMote, LoRaWAN eu868, Orange network (OTAA)
* [Semtech LR1120DVK1TCKS](https://www.semtech.fr/products/wireless-rf/lora-edge/lr1120dvk1tcks): Nucleo L476RG + LR1120 + Active GNSS antenna, [LoRaWAN eu868 (LR-FHSS)](https://github.com/Lora-net/SWSD003/tree/master/lr11xx/apps/lrfhss), TTN network (OTAA) 
* [Semtech LR1120DVK1TCKS](https://www.semtech.fr/products/wireless-rf/lora-edge/lr1120dvk1tcks): Nucleo L476RG + LR1120 + Passive GNSS antenna, [LoRaWAN eu868 (LR-FHSS)](https://github.com/Lora-net/SWSD003/tree/master/lr11xx/apps/lrfhss), TTN network (OTAA) 
* [Semtech LR1110TRK1BKS](https://www.semtech.fr/products/wireless-rf/lora-edge/lr1110trk1bks), LoRaWAN eu868, TTN (OTAA)
* [IMST iM880a + coreconf](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, TTN network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, TTN network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, Helium network (ABP)
* [Wyres Base](https://github.com/CampusIoT/RIOT-wyres), LoRaWAN eu868, Requea+Helium (ABP)
* [Wyres Base](https://github.com/CampusIoT/RIOT-wyres), LoRaWAN eu868, Requea+Helium (ABP)
* Echostar Mobile EVK, LR-FHSS SBand, [Echostar Mobile SatIoT GEO network](https://www.echostarmobile.com/)

### Balloon gondola #3

* CNES [Radio sonde M20](https://www.meteomodem.com/m20?lang=fr) 
* CNES IRMA GPS tracker (Iridium) 
* Nucleo F446RE  + RAK5146 (endpoint+repeater),  LoRaWAN eu868, TTN network (ABP)
* Adeunis Demomote, LoRaWAN eu868, Orange network (OTAA)
* [Semtech LR1120DVK1TCKS](https://www.semtech.fr/products/wireless-rf/lora-edge/lr1120dvk1tcks): Nucleo L476RG + LR1120 + Active GNSS antenna, [LoRaWAN eu868 (LR-FHSS)](https://github.com/Lora-net/SWSD003/tree/master/lr11xx/apps/lrfhss), TTN network (OTAA) 
* [Semtech LR1110TRK1BKS](https://www.semtech.fr/products/wireless-rf/lora-edge/lr1110trk1bks), LoRaWAN eu868, TTN (OTAA)
* [IMST iM880a + coreconf](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, TTN network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, TTN network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, Helium network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, Requea network (ABP)
* [Wyres Base](https://github.com/CampusIoT/RIOT-wyres), LoRaWAN eu868, Requea+Helium (ABP)
* [Wyres Base](https://github.com/CampusIoT/RIOT-wyres), LoRaWAN eu868, Requea+Helium (ABP)
* [ST Microlectronics P-Nucleo WL55JC2](https://www.st.com/en/evaluation-tools/nucleo-wl55jc.html), CLS 400 MHz, [Kineis SatIoT LEO network](https://www.kineis.com/)
* [ST Microlectronics B-L462E-CELL1 Cellular IoT Discovery kit](https://www.st.com/en/evaluation-tools/b-l462e-cell1.html) + SFR NB-IoT SIM

### On board cameras (ground, side and top balloon)

[SJCam SJ4000](https://www.sjcam.com/fr/cameras/action-cameras/sj4000-wifi/), [XTrem 4K](https://www.xtremcam.com/produit/camera-sport-hd-xtc-ultra-wifi-4k/), [Wolfang 4K](https://wolfang.co/), FF65/S (special thanks to Sébastien JEAN)


## Networks

### Terrestrial

* LoRaWAN: Orange, TheThingsNetwork, Helium, REQUEA, CampusIoT, TinyGS, Actility (to be confirmed)
* NB-IoT: SFR
* Radio sonde: [radiosondy](https://radiosondy.info/sonde_table.php?startplace=Aire+Sur+Adour%09%28FR%29&table=startplace) + sondehub.org 
  
### SatIoT
* [Echostar Mobile](https://echostarmobile.com/) (GEO)
* [Kineis](https://www.kineis.com/) (LEO)

## Mission scenario

### Endpoints `DevAddr`s

For roaming and TinyGS

Coming soon.

### Messages

The message Javascript decoder is [here](decoder/decoder.js).

### Two-way ranging (with fine timestamping) scenario

For the Two-way ranging (with fine timestamping) scenario, the gateway (registered on CampusIoT LNS) which responds to the [RANGING frames](messages/ranging.h) of the Thingsat EM and NucleoF446RE+RAK5146, is located at [Aiguille du Midi](https://en.wikipedia.org/wiki/Aiguille_du_Midi) at 3796 meters high. The distance Line-of-Sight between the balloon and the gateway is between 460 kms and 614 kms (926 kms by car).

The fine timestamping is enabled thanks to SX1303 and GNSS PPS.

The format of the ranging messages is [here](messages/ranging.h).

## Maps

Takeoff at [Aire-sur-l'Adour](https://www.openstreetmap.org/relation/76165#map=17/43.70644/-0.25125)


### Balloon #1 (Sonde id: 101 2 [00729](https://radiosondy.info/sonde_archive.php?sondenumber=ME8C00729))
Max altitude: 30798m (Min temperature: -52°C). Landing at 10:43CET[43.74389,0.27096](https://www.google.com/maps/place/43%C2%B044'38.0%22N+0%C2%B016'15.5%22E) (~42 kms from launching point)

![EOSCAN](media/eoscan-ncu2.png)

### Balloon #2 (Sonde id: 101 2 00733)
Max altitude: 32156m (Min temperature: -56°C). Landing at 10:50CET [43.72150,0.26801](https://www.google.com/maps/place/43%C2%B043'17.4%22N+0%C2%B016'04.8%22E) (~41 kms from launching point)

![EOSCAN](media/eoscan-ncu1.png)

### Balloon #3 (Sonde id: 101 2 [00734](https://radiosondy.info/sonde_archive.php?sondenumber=ME8C00734))
Max altitude: 26627m (Min temperature: -54°C). Landing at 10:55CET [43.71729,0.47152](https://www.google.com/maps/place/43%C2%B043'02.2%22N+0%C2%B028'17.5%22E) (~58 kms from launching point)

![EOSCAN](media/eoscan-ncu3.png)

### Coverage Map

Coming soon


#### LR-FHSS frame transmissions

![altitude of ballons when lr-fhss packet received](media/altitude_of_ballon_when_packet_lr-fhss_received.png)

## Results, Datasets and Analysis

* [LR-FHSS at LPWAN Days, Pau, 2024](https://cpham.perso.univ-pau.fr/LPWAN24/pdf/Presentation-Dobler-Rojas.pdf) [HAL citation](https://hal.science/hal-04777306)
* [LR-FHSS Datasets](https://github.com/CampusIoT/datasets/tree/main/LR-FHSS)

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [LCIS](https://lcis.fr/), [CROMA](https://croma.grenoble-inp.fr/)), [Carnot LSI](https://carnot-lsi.com/), [INSA de Lyon CITI](https://www.citi-lab.fr/), IMT Atlantique, ANS Innovation, [Slices FR](https://slices-fr.eu/), [CNES Centre des Opérations Ballons de Aire-sur-l'Adour](https://ballons.cnes.fr/fr), REQUEA, Orange, Echostar Mobile, Kineis, Semtech, ChipSelect, Université Clermont Auvergne [LPC](https://lpc-clermont.in2p3.fr/), [Observatoire du Puy de Dome](https://opgc.uca.fr/).

Special thanks to [Poissonnerie Lachenal - Air Marin](https://www.poissonnerie-lachenal.fr/) for the gondolas.

## Media

[Balloon burst (video)](media/burst.mp4)

[Enjoy](./media)

Gondolas setup
![Gondolas preparation](media/gondola-01.jpg)
![Gondolas preparation](media/gondola-02.jpg)
![Gondolas preparation](media/gondola-03.jpg)


D Day
![Balloon inflating](media/balloon_inflating-01.jpg)
![Balloon inflating](media/balloon_inflating-02.jpg)
![Balloon inflating](media/balloon_inflating-03.jpg)
![Gondola preparation](media/gondola_preparation-01.jpg)
![Gondola preparation](media/gondola_preparation-02.jpg)
![Gondola preparation](media/gondola_preparation-03.jpg)
![Gondola preparation](media/gondola_ready_to_fly.jpg)
![iBTS](media/ibts_for_lrfhss.jpg)
![Kerlink iBTS Gateway at Puy de Dome for the LR-FHSS test](media/ibts-puy-de-dome.jpg)
![Multi frequencies RX station](media/multi-frequencies-rx-station-01.jpg)
![Multi frequencies RX station](media/multi-frequencies-rx-station-02.jpg)
![Takeoff](media/takeoff-01.jpg)
![Takeoff](media/takeoff-02.jpg)
![Takeoff](media/takeoff-04.jpg)
![Takeoff](media/takeoff-05.jpg)
![Burst](media/burst-01.jpg)
![Burst](media/burst-02.jpg)
![Recovery](media/recovery.jpg)
![Recovery](media/recovery-02.jpg)

Kerlink iStation Gateway at Aiguille du Midi for the RANGING mission scenario
![Gateway at Aiguille du Midi for the RANGING mission scenario](https://campusiot.github.io/images/media/large/station-ige-aiguille-du-midi.jpg)

