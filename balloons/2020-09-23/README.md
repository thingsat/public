# Thingsat :: High altitude LoRa Benchmarking :: Results of the 2020-09-23 flight

## Aims
* Benchmarking LoRa links over very long distance (> 500 kms) for several frequency bands (433MHz, 868MHz, 2.4GHz).

## LoRa onboard endpoints
* Semtech LR1110 Eval Kit, LoRaWAN eu868, LiveObject network (OTAA)
* Semtech LR1110 Eval Kit, LoRaWAN eu868, TTN network (OTAA)
* Semtech LoRaMote, LoRaWAN eu868, LiveObject network (OTAA)
* Adeunis Demomote, LoRaWAN eu868, Orange network (OTAA)
* ST Microlectronics LRWAN3, LoRaWAN eu433, CampusIoT network (OTAA)
* Sagem Siconia, LoRaWAN eu868, LiveObject network (OTAA)
* IMST iM880a, LoRaWAN eu868, LiveObject (OTAA)
* IMST iM880a, LoRaWAN eu868, Objenious (OTAA)
* IMST iM880a, LoRaWAN eu868, CampusIoT (OTAA)
* ANS Lambda80, LoRaPHY ism2400, P2P
* ANS RN2483, LoRaPHY eu433, P2P

## Maps
![Map of balloons tracks and receivers](map-gw-tracks.png)
* Balloon #1's track in blue
* LiveObject's gateways in orange
* TTN's gateways (coming soon)
* Requea's gateways (coming soon)

![Map of receivers](map-gw.png)
Legend:
* LiveObject's gateways in orange
* TTN's gateways (coming soon)
* Requea's gateways (coming soon)

## Analysis
Coming soon

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [IMEP-LaHC](https://imep-lahc.grenoble-inp.fr/)), [ANS Innovation](https://www.ans-innovation.fr/), [Geditech](https://www.geditech.fr/), [CNES](https://cnes.fr/en) Division Ballon, [Orange LiveObject](https://liveobjects.orange-business.com/), [Requea](https://www.requea.com/), [RIOTOS team @ INRIA](https://www.riot-os.org/), [ST Microelectronics](https://www.st.com/content/st_com/en.html), Semtech, [Chipselect](http://chipselect.fr/). 

## Media
* [All ...](./media)

![Internal of case](./media/internal-1.jpg)
![Internal of case](./media/internal-6.jpg)
![Internal of case](./media/internal-3.jpg)
![Ground stations](./media/ground-stations.jpg)
![Launching](./media/launching-1.jpg)
![Launching](./media/launching-2.jpg)
![Recover](./media/recovering-1.jpg)
![Recover](./media/recovering-2.jpg)
