# Thingsat :: High altitude LPWAN/LPGAN Benchmarking :: November, 2nd, 2024, Grenoble

The balloon flight is operated by [Planete Sciences](https://www.planete-sciences.org/aura/) for the [tenth birthday of Planete Sciences AURA](xl_10ans.jpg)

## Aims

* Benchmarking LoRa links over very long distance (> 500 kms) for EU868 band (863-870 MHz).
* Benchmarking Narrow Band LoRa links over very long distance (> 500 kms) for EU868 band.

## Balloon gondola

The weight of the balloon gondola must weigh less than 2000 grams. The dimension is 27U (ie 30x30x30 cms). The material is expanded polystyrene.

* [Radio sonde M20](https://www.meteomodem.com/m20?lang=fr), [APRS packets](https://en.wikipedia.org/wiki/Automatic_Packet_Reporting_System) @ FSK 404.015 MHz
* Semtech LoRaMote, LoRaWAN eu868, Orange network (OTAA)
* ANS Innovation STM32WL55 868 MHz endpoint, Orange network (ABP)
* ANS Innovation STM32WL55 868 MHz endpoint (Narrow Band LoRa on 869.525 MHz) (ABP)
* [L0adRa-Tracker](https://github.com/l0ad/ubpe-valence-LoRa), LoRaWAN eu868, TTN network (ABP)
* ~~Adeunis Demomote, LoRaWAN eu868, Orange network (OTAA)~~
* ~~Flipper Zero + Wio LoRa E5,  LoRaWAN eu868, CampusIoT network (OTAA)~~
  
## Networks

### Terrestrial

* LoRaWAN: Orange, CampusIoT, TTN
* Radio sonde: [radiosondy](https://radiosondy.info) + [sondehub.org](https://sondehub.org)

Last Orange gateway for gondola recovery : [45.71333/5.28139] ([Site 2618833, LD ROSSETEDF MONT ROSSET, 38460 DIZIMIEU](https://www.cartoradio.fr/#/cartographie/lonlat/5.285645/45.715938)) 

### SatIoT DtS (Direct-to-Satellite)

None

### Endpoints `DevAddr`s

For roaming and TinyGS
| Name | DevAddr | AppSKey | Comment |
| ---- | ------- | ------- | ------- |
| ANS Tracker | `1e75d619` | `cf7d3ea8ee0a8eb388c1d566f084b74a` | |
| L0adRa-Tracker | `260b79f7` | `5EC6412CE6D5ACB0107BAD24E1D061E2` | |
| Semtech LoRaMote |  | | OTAA Never join :disappointed_relieved: |
| ~~Adeunis Demomote~~ | | | OTAA Stay on ground since not enought weight budget |

M20 radio sonde ID: `101-2-00734`

### Messages



## Maps

### Actual Trajectory

Takeoff from [Place de la Commune de 1871, Grenoble](https://www.openstreetmap.org/search?query=Place%20de%20la%20Commune%20de%201871%2C%20Grenoble#map=18/45.178862/5.745072)

Burst at 29263.2m ([45.44485,5.39639](https://www.openstreetmap.org/search?query=45.44485%2C5.39639#map=18/45.444850/5.396390)).

Landing at [45.51060,5.30780](https://www.openstreetmap.org/#map=15/45.51060/5.30780)

![Trajectory](./media/021124-path.jpg)

Gondola recovered after nightfall, in foggy conditions, in a middle of a field, no sign of parachute.

### Prediction

![Track prediction](./media/predict-30000.jpg)

[Track prediction](./files/021124-Prediction-burst30k.kml) (burst at 30000m) by https://predict.sondehub.org/

![Actual Vs  prediction](./media/trajVsPredict.jpg) 

- slower ascent, due to an heavy gondola and maybe not enough Helium.
- faster descent, due to parachute separation at nearly 17000m (see flight profile above)


### Coverage Map

Coming soon

## Results, Datasets and Analysis

- Flight log (trajectory + sensor data): [CSV](./files/021124-L0adraTrackerLog.csv)
- Trajectory: [GeoJSON](./files/021124-path.geojson)

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [LCIS](https://lcis.fr/), [CROMA](https://croma.grenoble-inp.fr/)), [ANS Innovation](https://www.ans-innovation.fr/), [Planète Sciences AURA](https://www.planete-sciences.org/aura/), [L0AD](https://www.facebook.com/LeL0AD/?locale=fr_FR), Orange LiveObject.

## Media
Payload 
![Payload](./media/payload.jpg)

Balloon inflating 
![Balloon inflating](./media/inflating.jpg)

TakeOff 
![TakeOff](./media/takeoff.jpg)

Chase 
![Chase](./media/chase.jpg)
![Chase2](./media/chase2.jpg)

Retrieval 
![Found](./media/found.jpg)
![Found2](./media/found2.jpg)

Kerlink iStation Gateway at Aiguille du Midi
![Gateway at Aiguille du Midi](https://campusiot.github.io/images/media/large/station-ige-aiguille-du-midi.jpg)


