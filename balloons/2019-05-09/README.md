# Thingsat :: High altitude LoRa Benchmarking :: Results of the 2019-05-09 flight

## Aims
* Benchmarking LoRa links over very long distance (> 500 kms) on 868MHz.

## LoRa onboard endpoints
* Adeunis Demomote, LoRaWAN eu868, Orange network (OTAA)
* IMST iM880a, LoRaWAN eu868, LiveObject (OTAA) + TTN (ABP)

Firmware:
* [Field Test Device](https://github.com/CampusIoT/orbimote/tree/master/field_test_device)

## Maps
![Map of the balloon track](./media/balloon-track+gw.png)
![Map of the balloon track](./media/balloon-track-3d.png)
Legend:
* Balloon's track in blue
* Orange gateways in red


![Map of Orange gateways](./media/map-orange.png)
Legend:
* LiveObject's gateways in grey

![Map of TTN gateways](./media/map-ttn.png)
Legend:
* TTN's gateways in blue

## Analysis
* [Analyses (in french)](analyses_lora_balloon.fr.pdf)
* [Distribution Frames vs LoS distance (per SF, PER, Power)](distribution_frame_vs_los_distance_per_sf_per_power.pdf)
* [Histogram Frames vs Time (per SF, PER, Power)](histogram_paquet_vs_time_per_sf_per_power.pdf)
* [RSSI vs Time (per SF, PER, Power)](rssi_vs_time_per_sf_per_power.pdf)

## Partners
Université Grenoble Alpes, IUT de Valence with Planete Sciences, Orange LiveObject.

## Media
* [All ...](./media)

![Field test device](./media/field-test-device.jpg)
![Balloon case](./media/balloon-case.jpg)
![LoRa Sniffer Car](./media/lorasniffercar-1.jpg)
![LoRa Sniffer Car](./media/lorasniffercar-2.jpg)
![Launching](./media/balloon_launching.jpg)
![Landing](./media/balloon-landing.png)
![Recover](./media/balloon_recovering.jpg)
