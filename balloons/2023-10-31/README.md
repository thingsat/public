# Thingsat :: High altitude LPWAN/LPGAN Benchmarking :: Results of the 2023-10-31 flight

## Aims

* Testing the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space conditions (vaccum and -60°C).
* Testing the [REPEAT mission scenario](../../cubesat_mission_2/mission_scenario/README.md#repeat868-mission-scenario) of the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space.
* Testing the [RANGING mission scenario](../../cubesat_mission_2/mission_scenario#mission-scenario) of the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space.
* Testing fragmented file transfert from ground to balloon and from balloon to ground using [XOR frame redundancy](../../cubesat_mission_2/messages/lorawan_payload.h?ref_type=heads#L48).
* [Collecting statistics on frames sent by ground LoRaWAN endpoints](../../cubesat_mission_2/cubesat_mission_2/mission_scenario/README.md#rx868-mission-scenario) from the near-space.
* Benchmarking LoRa links over very long distance (> 500 kms) for EU868 bands (868MHz).
* Benchmarking cellular IoT links (NB-IoT) at high-altitude (up to 30 kms).
* Benchmarking SatIoT communications for high-altitude balloon tracking

## Balloon gondola

The weight of the balloon basket must weigh less than 2000 grams. The material is expanded polystyrene.

### Onboard gateway
* Thingsat cubesat hosted payload (SX1303-based LoRaWAN gateway for cubesat) + [Adafruit Ultimate GPS](https://www.adafruit.com/product/746)
  
### Onboard endpoints
* Semtech LoRaMote, LoRaWAN eu868, Orange network (OTAA)
* Semtech LoRaMote, LoRaWAN eu868, TTN network (OTAA)
* Adeunis Demomote, LoRaWAN eu868, Orange network (OTAA)
* [Seeedstudio LoRa E5 Mini](https://www.seeedstudio.com/LoRa-E5-mini-STM32WLE5JC-p-4869.html), LoRaWAN/LR-FHSS eu868, CampusIoT network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, Orange network (OTAA)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, TTN network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, Helium network (ABP)
* [IMST iM880a](https://github.com/CampusIoT/tutorial/blob/master/im880a/README.md), LoRaWAN eu868, Requea network (ABP)
* [Wyres Base](https://github.com/CampusIoT/RIOT-wyres), LoRaWAN eu868, Orange network (OTAA)
* [ST Microlectronics P-Nucleo WL55JC2](https://www.st.com/en/evaluation-tools/nucleo-wl55jc.html), CLS 400 MHz, [Kineis SatIoT LEO network](https://www.kineis.com/)
* Echostar Mobile EVK, LR-FHSS SBand, [Echostar Mobile SatIoT GEO network](https://www.echostarmobile.com/)
* [ST Microlectronics B-L462E-CELL1 Cellular IoT Discovery kit](https://www.st.com/en/evaluation-tools/b-l462e-cell1.html) + SFR NB-IoT SIM

Endpoints and gateway include sensors such as temperature, humidity, atmospheric pressure, accelerometer, magnetometer and gyroscope.

## Networks

### Terrestrial

* LoRaWAN: Orange, TheThingsNetwork, Helium, REQUEA, CampusIoT
* NB-IoT: SFR

### SatIoT
* Echostar Mobile (GEO)
* Kineis (LEO)

## Mission scenario

### RANGING mission scenario

For the RANGING mission scenario, the gateway (registered on CampusIoT LNS) which responds to the RANGING resquests of the Thingsat EM, is located at [Aiguille du Midi](https://en.wikipedia.org/wiki/Aiguille_du_Midi) at 3796 meters high. The distance Line-of-Sight between the balloon and the gateway is between 460 kms and 614 kms (926 kms by car).

The format of the RANGING resquest and response is [here](messages/ranging.h).

## Maps

### Track

Takeoff at Aire-sur-l'Adour (in green) and Landing at Lagardiolle (in red). Distance : 192 kms

Max altitude (in yellow): 29544 meters (-53.89°C, 12.3hPa) @ 31 October 2023 10:12:50 GMT (43.6162558626,1.3647286602)

Balloon tracks and trackers' sensors data
![Track GPS](media/track.jpg)
![EOSCAN](media/EOSCAN.png)
![IRMA](media/IRMA.png)

### Ground gateways coverage

![Orange gateways](media/coverage.png)
Orange (in orange), TTN (in blue), Helium (in dark green), Requea (coming soon), CampusIoT (coming soon)

## Datasets and analysis

Coming soon

[Radio sonde ME8C00666](https://radiosondy.info/sonde_archive.php?sondenumber=ME8C00666)

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [IMEP-LaHC](https://imep-lahc.grenoble-inp.fr/)), [CNES Centre des Opérations Ballons de Aire-sur-l'Adour](https://ballons.cnes.fr/fr), REQUEA, Orange, Echostar Mobile, Kineis, Université Clermont Auvergne, ANS Innovation.

## Media

[Enjoy](./media)

[Take off (video)](media/takeoff.m4v)

Gondola internal
![Nacelle](media/nacelle-01.jpg)

Thingsat FM on board
![Thingsat FM on board](media/thingsat_fm_onboard.jpg)

Gondola setup just before the flight
![Level 0](media/gondola_level_0.jpg)
![Level 1](media/gondola_level_1.jpg)

Balloon setup just before the flight
![Inflating](media/inflating_the_balloon_with_helium.jpg)
![Installation](media/balloon_installation_2.jpg)
![Installation](media/balloon_installation_1.jpg)

Balloon take off
![Takeoff](media/takeoff.jpg)

Gondola recovery by a farmer
![Recovrey](media/gondola_recovered_by_a_farmer.jpg)
![Recovrey](media/gondola_after_landing.jpg)

Gateway at Aiguille du Midi for the RANGING mission scenario
![Gateway at Aiguille du Midi for the RANGING mission scenario](https://campusiot.github.io/images/media/large/station-ige-aiguille-du-midi.jpg)
