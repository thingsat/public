/*
 Thingsat project

 Copyright (c) 2021-2023 UGA CSUG LIG

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */


/*
 * Thingsat Mission Scenarii :: fpaylaod definition for RANGING mission scenario
 */

/*
 * Author : Didier Donsez, Université Grenoble Alpes
 */


#ifndef THINGSAT_RANGING_H
#define THINGSAT_RANGING_H

#include <stdint.h>

#ifndef REPEATER_TX_RANGING_FPORT
#define REPEATER_TX_RANGING_REQUEST_FPORT		(160U)
#endif

#ifndef REPEATER_TXRX_RANGING_RESPONSE_FPORT
#define REPEATER_TXRX_RANGING_RESPONSE_FPORT	(161U)
#endif

/*
 * Ranging request
 */
struct __attribute__((__packed__)) ranging_request {

	/**
	 * @brief SX130x internal counter
	 */
	uint32_t counter;

	/**
	 * @brief Latitude	(in degree)
	 * optional field
	 * TODO optimization on int24
	 */
	float latitude;

	/**
	 * @brief Longitude (in degree)
	 * optional field
	 */
	float longitude;

	/**
	 * @brief Altitude (in meter)
	 * optional field
	 * TODO optimization on int24
	 */
	uint16_t altitude;

};

typedef struct ranging_request ranging_request_t;

struct __attribute__((__packed__)) ranging_response {

	/**
	 * @brief DevAddr of the ranging_request
	 */
	uint32_t devaddr;

	/**
	 * @brief fCnt of the ranging_request
	 */
	uint16_t fCnt;

	/**
	 * @brief SX130x internal counter of the ranging_request
	 */
	uint32_t counter;

	/**
	 * @brief Timestamp (seconds since epoch) at the reception of the ranging_request
	 * optional field
	 */
	uint32_t timestamp_sec;

	/**
	 * @brief Timestamp (nanoseconds) at the reception of the ranging_request
	 * optional field
	 */
	uint32_t timestamp_nsec;

	/**
	 * @brief Latitude	(in degree)
	 * optional field
	 * TODO optimization on int24
	 */
	float latitude;

	/**
	 * @brief Longitude (in degree)
	 * optional field
	 * TODO optimization on int24
	 */
	float longitude;

	/**
	 * @brief Altitude (in meter)
	 * optional field
	 */
	uint16_t altitude;

};

typedef struct ranging_response ranging_response_t;

size_t fill_ranging_request(ranging_request_t *rq);

bool process_ranging_response(ranging_response_t *rq);

void print_ranging_request(ranging_request_t *rq);

void print_ranging_response(ranging_response_t *rq);

#endif
