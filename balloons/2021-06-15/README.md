# Thingsat :: High altitude LoRa Benchmarking :: Results of the 2021-06-15 flight

## Aims
* Benchmarking LoRa links over very long distance (> 500 kms) for several frequency bands (868MHz).
* Studying frames of ground LoRaWAN endpoints from the near-space.

## LoRa onboard endpoints and onboard gateways
### Balloon
* Adeunis Demomote, LoRaWAN eu868, Orange network (OTAA)

## Maps
![Orange gateways](orange_gateways.png)
Legend:
* Orange's gateways in orange

![Track](track.png)
Legend:
* Lauching point in green
* Landing point in red
* Track in blue
* Orange's gateways in orange

## Analysis
Coming soon

## Misc

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) IUT de Valence, Lycée Triboulet (Roman sur Isère), Planete Sciences, ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [IMEP-LaHC](https://imep-lahc.grenoble-inp.fr/)).

