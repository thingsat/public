# Thingsat :: High altitude LPWAN/LPGAN Benchmarking :: Spring 2025

## Aims

* Testing the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space conditions (vaccum and -60°C).
* Testing the [REPEAT mission scenario](../../cubesat_mission_2/mission_scenario/README.md#repeat868-mission-scenario) of the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space.
* Testing the [Two-way ranging (with fine timestamping)](../../cubesat_mission_2/mission_scenario#mission-scenario) of the [Thingsat cubesat hosted payload](../../cubesat_mission_2) in near-space.
* Testing fragmented file transfert from ground to balloon and from balloon to ground using [XOR frame redundancy](../../cubesat_mission_2/messages/lorawan_payload.h?ref_type=heads#L48).
* Benchmarking LR-FHSS links over very long distance (> 500 kms) for EU868 bands (868 MHz). With CITI Lab
* Benchmarking Mioty links over very long distance (> 500 kms) for EU868 bands (868 MHz). With Franhoffer IIS
* Benchmarking Narrow Band LoRa links over very long distance (> 500 kms) for EU868 bands (433, 868, 2400 MHz).
* Benchmarking LoRa links over very long distance (> 500 kms) for EU868 bands (868MHz).
* Benchmarking cellular IoT links (NB-IoT) at high-altitude (up to 30 kms).
* Benchmarking SatIoT communications for high-altitude balloon tracking
* Testing LoRaWAN roaming for high-altitude balloon. With Actility, Requea and Helium.
* Testing the setup of I/Q replay with SDR for high-altitude balloon. With Kineis and INRIA CITI (ANR STEREO).
* Testing LoRaWAN [CORECONF](https://datatracker.ietf.org/doc/draft-ietf-core-comi/) frames for high-altitude balloon. With IMT Atlantique.
* Testing [Meshtastic](https://meshtastic.org/) routing protocol from high-altitude balloons. With [french and spanish Meshtastic communities](https://meshtastic.org/docs/community/local-groups/).
* Testing GNSS modules from high-altitude balloon
* Testing LoRa-Edge RTLS from high-altitude balloon (LR11xx)
* Testing [Kikiwi2 balloon trackers](https://gricad-gitlab.univ-grenoble-alpes.fr/thingsat/seed/-/tree/main/seed_balloon) from high-altitude balloons

## Balloon gondolas

The weight of the balloon basket must weigh less than 2800 grams. The material is expanded polystyrene.

Endpoints and gateway include sensors such as temperature, humidity, atmospheric pressure, accelerometer, magnetometer and gyroscope.

Some GNSS modules are configured in balloon mode.

CNES Radio sondes send [APRS packets](https://en.wikipedia.org/wiki/Automatic_Packet_Reporting_System) at 404.002 MHz

4K sportcams are on-board for shooting the flight.

### Balloon gondola #1

Coming soon

### Balloon gondola #2

Coming soon

### Balloon gondola #3

Coming soon

### On board cameras (ground, side and top balloon)

[SJCam SJ4000](https://www.sjcam.com/fr/cameras/action-cameras/sj4000-wifi/), [XTrem 4K](https://www.xtremcam.com/produit/camera-sport-hd-xtc-ultra-wifi-4k/), [Wolfang 4K](https://wolfang.co/), FF65/S (special thanks to Sébastien JEAN)

## Networks

### Terrestrial

* LoRaWAN: Orange, TheThingsNetwork, Helium, REQUEA, CampusIoT, TinyGS, Actility (to be confirmed)
* NB-IoT: SFR
* Radio sonde: [radiosondy](https://radiosondy.info/sonde_table.php?startplace=Aire+Sur+Adour%09%28FR%29&table=startplace) + sondehub.org
* Meshtastic
* TinyGS

### SatIoT
* [Echostar Mobile](https://echostarmobile.com/) (GEO)
* [Kineis](https://www.kineis.com/) (LEO)

## Mission scenario

### Endpoints `DevAddr`s / `Source_Id`

For roaming, TinyGS, Meshtastic

Coming soon.

### Messages

The message Javascript decoder is [here](decoder/decoder.js).

### Two-way ranging (with fine timestamping) scenario

For the Two-way ranging (with fine timestamping) scenario, the gateway (registered on CampusIoT LNS) which responds to the [RANGING frames](messages/ranging.h) of the Thingsat EM and NucleoF446RE+RAK5146, is located at [Aiguille du Midi](https://en.wikipedia.org/wiki/Aiguille_du_Midi) at 3796 meters high. The distance Line-of-Sight between the balloon and the gateway is between 460 kms and 614 kms (926 kms by car).

The fine timestamping is enabled thanks to SX1303 and GNSS PPS.

The format of the ranging messages is [here](messages/ranging.h).

## Tracks

Takeoff at [Aire-sur-l'Adour](https://www.openstreetmap.org/relation/76165#map=17/43.70644/-0.25125)

Coming soon. Live tracking on https://radiosondy.info/ and https://sondehub.org/#!mt=Mapnik&mz=9&qm=3h&mc=43.82363,-0.12634&box=aboutbox

### Coverage Map

Coming soon

## Results, Datasets and Analysis

Coming soon

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [LCIS](https://lcis.fr/), [CROMA](https://croma.grenoble-inp.fr/)), [Carnot LSI](https://carnot-lsi.com/), [INSA de Lyon CITI](https://www.citi-lab.fr/), IMT Atlantique, ANS Innovation, [Slices FR](https://slices-fr.eu/), [CNES Centre des Opérations Ballons de Aire-sur-l'Adour](https://ballons.cnes.fr/fr), CNES Nanolab Academy, REQUEA, Orange, Echostar Mobile, Kineis, Semtech, ChipSelect, Le Climatographe, [Fraunhofer IIS](https://www.iis.fraunhofer.de/en.html), UCA LEAT.

Special thanks to [Poissonnerie Lachenal - Air Marin](https://www.poissonnerie-lachenal.fr/) for the gondolas.

## Media

Coming soon

ThingSat @ INISAT 📡 🚀 (LoRa gateways)
![ThingSat @ INISAT 📡 🚀](https://raw.githubusercontent.com/csu-grenoble/flatsat/refs/heads/main/Hardware/Thingsat_INISAT/images/inisat-l432kc%2Brak5146%2Blambda80-01.jpg)

ProtoSEED with LoRa endpoints (868 MHz and 2.4 GHz)
![ProtoSEED](https://gricad-gitlab.univ-grenoble-alpes.fr/thingsat/seed/-/raw/main/media/seed-protoboards.jpg)

Kerlink iStation Gateway at Aiguille du Midi for the RANGING mission scenario
![Gateway at Aiguille du Midi for the RANGING mission scenario](https://campusiot.github.io/images/media/large/station-ige-aiguille-du-midi.jpg)

