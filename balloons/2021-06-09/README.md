# Thingsat :: High altitude LoRa Benchmarking :: Results of the 2021-06-09 flight

## Aims
* Benchmarking LoRa links over very long distance (> 500 kms) for several frequency bands (868MHz).
* Studying frames of ground LoRaWAN endpoints from the near-space.

## LoRa onboard endpoints and onboard gateways
### Balloon
* [IMST iM880a](https://github.com/CampusIoT/orbimote/tree/master/field_test_device#default-board), LoRaWAN eu868, TTNv2 network (ABP)

## Maps
![TTNv2 gateways](ttn_gateways.png)
Legend:
* TTNv2's gateways in blue
![Flight forecast](forecast.jpg)

## Analysis
Coming soon

## Misc
### Live dashboard and Logging 
* NodeRED (coming soon)
* Grafana (coming soon)

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [IMEP-LaHC](https://imep-lahc.grenoble-inp.fr/)), [ANS Innovation](https://www.ans-innovation.fr/), [CNES](https://cnes.fr/en) Division Ballon.

## Media
![Board 1](board-1.jpg)
![Board 2](board-2.jpg)
