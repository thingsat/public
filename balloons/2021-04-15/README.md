# Thingsat :: High altitude LoRa Benchmarking :: Results of the 2021-04-15 flights

## Aims
* Benchmarking LoRa links over very long distance (> 500 kms) for several frequency bands (433MHz, 868MHz, 2.4GHz).
* Studying frames of ground LoRaWAN endpoints from the near-space.
* Testing LoRaWAN roaming between a private network and public networks (Swisscom, KPN) thru the Actility roaming hub.
* Repeating LoRa frames of ground LoRaWAN trackers (emergency usecase).

## LoRa onboard endpoints and onboard gateways
### Balloon #1
* [SX1302C868GW1 Corecell Reference Design](https://www.semtech.com/products/wireless-rf/lora-core/sx1302cxxxgw1) (RPI + Semtech Corecell eval kit based on SX1302 and SX125), LoRaWAN eu868 
* Elsys EMS, LoRaWAN eu868, Actility roaming hub (ABP)
* Semtech LR1110 Eval Kit, LoRaWAN eu868, LiveObject network (OTAA)
* Adeunis Demomote, LoRaWAN eu868, Orange network (OTAA)
* ST Microlectronics LRWAN3, LoRaWAN eu433, CampusIoT network (OTAA)
* IMST iM880a, LoRaWAN eu868, CampusIoT (OTAA)
* IMST iM880a, LoRaWAN eu868, Actility roaming hub (ABP)

### Balloon #2
* Elsys EMS, LoRaWAN eu868, Actility roaming hub (ABP)
* Semtech LoRaMote x2, LoRaWAN eu868, CNES network (OTAA)
* TTGO, LoRaWAN eu868, CNES network (OTAA)
* IMST iM880a x2, LoRaWAN eu868, Actility roaming hub (ABP)
* ANS Lambda80, LoRaPHY ism2400, P2P
* ANS RN2483, LoRaPHY eu433, P2P
* ANS RN2483, LoRaPHY eu868, P2P

### Balloon #3
* RPI + Corecell eval kit, LoRaWAN eu868
* Elsys EMS, SemtechLoRaWAN eu868, Actility roaming hub (ABP)
* Semtech LR1110 Eval Kit, LoRaWAN eu868, LiveObject network (OTAA)
* Adeunis Demomote, LoRaWAN eu868, Orange network (OTAA)
* ST Microlectronics P-Nucleo WL55, LoRaWAN eu433, CampusIoT network (OTAA)
* ST Microlectronics P-Nucleo WB55 + Semtech LLCC68, LoRaWAN eu868, Orange network (OTAA)
* ST Microlectronics Nucleo F411RE + Lambda80, LoRaWAN ism2400, CampusIoT network (ABP)
* IMST iM880a, LoRaWAN eu868, Orange network (OTAA)
* IMST iM880a, LoRaWAN eu868, TTNv3 network (OTAA)
* IMST iM880a, LoRaWAN eu868, CampusIoT network (OTAA)
* IMST iM880a, LoRaWAN eu868, Actility roaming hub (ABP)

### Ground
* Kerlink iStation + Barracuda Taoglas 12 dBi, eu868, CampusIoT network
* Kerlink iStation + Barracuda Taoglas 12 dBi, eu868, CNES network
* TTGI indoor, eu868, TTNv2 network
* Rak Wireless indoor, eu433, CampusIoT network
* Multitech MTCDT + 2G4-3 mCard, ism2400, CampusIoT network
* Multitech MTCDT + 868 MHz mCard, eu868, CNES network (in car)
* ANS Lambda80, LoRaPHY ism2400, P2P
* ANS RN2483, LoRaPHY eu433, P2P
* ANS RN2483, LoRaPHY eu868, P2P

## Ground gateways
![Map of receivers](map-gw.png)
Legend:
* LiveObject's gateways in orange
* Swisscom's gateways (thru the Actility roaming hub) in red
* Requea's gateways in blue navy
* CampusIoT's gateways (in green @ Aire-Sur-L'Adour)

## Balloons tracks
![Map of balloons tracks and receivers](map-gw-tracks.png)
* Balloon #1's track in blue (max. altitude : 30913 meters in yellow, min. temperature: -60.1°C)
* Balloon #2's track in cyan (max. altitude : 28349 meters in yellow, min. temperature: -59.2°C)
* Balloon #3's track in purple (max. altitude : 28676 meters in yellow, min. temperature: -59.9°C)
* LiveObject's gateways in orange
* CampusIoT's gateways in green @ Aire-Sur-L'Adour

## Analysis
Coming soon

## Misc
### Live dashboard and Logging 
* NodeRED (coming soon)
* Grafana (coming soon)

### SX1302C868GW1 Corecell repeater, logger and forwarder 
* [LoRa repeater](https://github.com/CampusIoT/lora-repeater) 

### Endpoints firmware
* [Field Test Device](https://github.com/CampusIoT/orbimote/tree/master/field_test_device)

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [IMEP-LaHC](https://imep-lahc.grenoble-inp.fr/)), [ANS Innovation](https://www.ans-innovation.fr/), [Geditech](https://www.geditech.fr/), [CNES](https://cnes.fr/en) Division Ballon, [Orange LiveObject](https://liveobjects.orange-business.com/), [Actility](https://www.actility.com/), [Swisscom](https://www.swisscom.ch/en/business/enterprise/offer/iot/lpn.html), [KPN](https://www.kpn.com/zakelijk/internet-of-things/en/lora-connectivity.htm), [Requea](https://www.requea.com/), [RIOTOS team @ INRIA](https://www.riot-os.org/), [ST Microelectronics](https://www.st.com/content/st_com/en.html), [Chipselect](http://chipselect.fr/). 

Special thanks to the [Lachenal fish shop](https://poissonnerie-lachenal.fr/) for the expanded polystyrene boxes.

## Media

* [All ...](./media)

![Internal of case #3](./media/case3-internal-floor.jpg)
![Internal of case #3](./media/case3-internal-top.jpg)
![Preparation of case #2](./media/case2.jpg)
![Launching](./media/launching-1.jpg)
![Launching](./media/launching-3.jpg)
![Launching](./media/launching-2.jpg)
![Ground stations](./media/ground-stations-1.jpg)
![Balloon RF Tracking](./media/rf-tracking.jpg)
![Cases are fortunatally recovered](./media/cases-recovered.jpg)


