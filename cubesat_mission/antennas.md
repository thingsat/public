# Antennas for ground stations and ground endpoints


## 868 MHz

| Brand | Ref | Band | Gain | Polarization | Aperture | Size | Price |
| ----- | --- | ---- | ---- | ------------ | -------- | ---- | ----- |
| RFThings | [RT-CP3-BL RFThings Edition](https://rfthings.com.vn/product/rt-cp3-bl-rfthings-edition/)  | 868 MHz |   | Circular  | 110° | 73 x 73 x 15 (mm) | 17$ |
|   |   |   |   |   |   |  | |





## 2.4 GHz

| Brand | Ref | Band | Gain | Polarization | Aperture | Size | Price |
| ----- | --- | ---- | ---- | ------------ | -------- | ---- | ----- |
|   | [Antenne Panneau 2.4 GHz 8 dBi](https://www.mhzshop.com/shop/fr/WIFI/Antennes-2-4-GHz/Panneaux-2-4-GHz/Antenne-Panneau-2-4-GHz-8-dBi-avec-cable-5m.html) | 2400  | 8  |  ? | 70° (H) 70° (V) |  | 24€ |
|   |   |   |   |   |   |   |   |

