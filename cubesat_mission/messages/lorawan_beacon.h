/*
 * Copyright (C) 2020-2022 Université Grenoble Alpes
 */

/*
 * Author : Didier Donsez, Université Grenoble Alpes
 */

/*
 * Thingsat Mission Scenarii :: Data structures
 */

/*
 * Types for the format of the beacons send by the Thingsat payload
 */


#ifndef LORAWAN_BEACON_H
#define LORAWAN_BEACON_H

#include <stdint.h>
#include <stdbool.h>


/**
 * @brief Beacon SF12 Message Payload
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The CRC SHALL use the polynomial P(x) = x16 + x12 + x5 + x0
 */
struct __attribute__((__packed__)) BeaconSF12MessagePayload
{
	uint8_t rfu1[4];
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint16_t crc1;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint8_t rfu2[3];
	uint16_t crc2;
};

typedef struct BeaconSF12MessagePayload BeaconSF12MessagePayload_t;

/**
 * @brief Beacon SF11 Message Payload
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The CRC SHALL use the polynomial P(x) = x16 + x12 + x5 + x0
 */
struct __attribute__((__packed__)) BeaconSF11MessagePayload
{
	uint8_t rfu1[3];
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint16_t crc1;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint8_t rfu2[2];
	uint16_t crc2;
};

typedef struct BeaconSF11MessagePayload BeaconSF11MessagePayload_t;

/**
 * @brief Beacon SF10 Message Payload
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The CRC SHALL use the polynomial P(x) = x16 + x12 + x5 + x0
 */
struct __attribute__((__packed__)) BeaconSF10MessagePayload
{
	uint8_t rfu1[2];
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint16_t crc1;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint8_t rfu2[1];
	uint16_t crc2;
};

typedef struct BeaconSF10MessagePayload BeaconSF10MessagePayload_t;

/**
 * @brief Beacon SF9 Message Payload
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The CRC SHALL use the polynomial P(x) = x16 + x12 + x5 + x0
 */
struct __attribute__((__packed__)) BeaconSF9MessagePayload
{
	uint8_t rfu1[1];
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint16_t crc1;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint16_t crc2;
};

typedef struct BeaconSF9MessagePayload BeaconSF9MessagePayload_t;

/**
 * @brief Beacon SF8 Message Payload
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The CRC SHALL use the polynomial P(x) = x16 + x12 + x5 + x0
 */
struct __attribute__((__packed__)) BeaconSF8MessagePayload
{
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint16_t crc1;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint8_t rfu2[3];
	uint16_t crc2;
};

typedef struct BeaconSF8MessagePayload BeaconSF8MessagePayload_t;



#endif
