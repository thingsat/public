# Thingsat :: LoRaWAN Messages and Payloads Formats

Message format drafts are described into the C header files.

[codec](./codec) directory contains decoders and encoders of messages and payloads sent by the Thingsat cubesat. [decoder.js](./codec/decoder.js) can be deployed on TTN, Chirpstack and Helium for decoding the payload at LNS level.
