# Thingsat :: LoRa and SatIoT benchmarks from cubesat

[ThingSat project](https://www.csug.fr/main-menu/projects/thingsat-internet-of-isolated-objects-by-satellite--929824.kjsp) developed by [CSUG](https://www.csug.fr/en/) and its [partners](#partners) aims to create a CubeSat payload dedicated to design and to benchmark long-distance communication protocols with low-power consumption LoRa modulation for Bidirectional Satellite IoT services (SatIoT) and Low Power Global Area Networks (LPGAN) ([publications and presentations](../publications.md)).

## Keywords

LoRa, LoRaWAN, Bidirectional SatIoT, LPGAN, Secure Firmware Update over the Space, CubeSat, New Space

## Aims

* Communications
  * Benchmarking LoRa links over very long distance (> 500 kms) for several frequency bands (868MHz, 2.4GHz) including immunity to Doppler effect.
  * Studying frames of ground LoRaWAN endpoints from the space.
  * Receiving frames sent by cubesats using ground terestrial LoRaWAN stations.
  * Designing and evaluating  LoRa [delay-tolerant networks](https://en.wikipedia.org/wiki/Delay-tolerant_networking) algorithms (store-and-forward principle).

* Applications
  * Synchronizing ground LoRaWAN endpoints clocks and constellation orbits parameters (ie TLE) from the space.
  * Repeating LoRa frames of ground LoRaWAN trackers (emergency usecase).
  * Monitoring with sensors in real field cases : artic glaciers, Helium tank containers, fish farms, young turtle tracking ...

Moreover, we tests new security features for firmware updates over the space.

![Delay Tolerant Network](./media/thingsat-dtn.png)

## Test fields
* Glaciers monitoring (French Alps & Ny Alesund, Svalbard)
* Fish farming, Young turtle tracking (French Polynesia)
* Secure clock synchronization (Europe, Africa, French Polynesia, Oceans)
* Helium container maritime shipping (Oceans)

## Launch
January 13th, 2022, 15:25 UTC on SpaceX Falcon 9 Block 5 Transporter 3 SSO Ride [Re-Watch](https://www.youtube.com/watch?v=mFBeuSAvhUQ). 

Transporter-3 is SpaceX’s third dedicated rideshare mission, and on board this launch are 105 spacecraft (including [CubeSats, microsats, PocketQubes](https://twitter.com/tiny__GS/status/1481315314411327488), and orbital transfer vehicles). Stork-1 has been carried by [D-Orbit's ION Satellite Carrier](https://www.dorbit.space/) and released on its orbit (January 31st , 2022, at 07:25:29 AM UTC).

Follow Thingsat on [Linkedin](https://www.linkedin.com/company/csug).

> [Falcon 9 Launch Weather Criteria](https://www.nasa.gov/pdf/649911main_051612_falcon9_weather_criteria.pdf) 


## Orbit

    Apogee: 525 km +/- 25 km
    Perigee: 525 km +/- 25 km
    Inclination: 97.5 deg +/- 0.1 deg
    MLTDN: 09:30 – 10:30


STORK-1 (NORAD ID 51087) : [TLE](https://www.n2yo.com/database/?q=STORK-1#results), [n2yo](https://www.n2yo.com/satellite/?s=51087), [Satflare](http://www.satflare.com/track.asp?q=51087#TOP)

> TLE (2023-06-27). Decay at 2024-04-29 
```
1 51087U 22002DH  23178.07690391  .00027050  00000-0  10027-2 0  9999
2 51087  97.4452 245.8080 0010475 199.6001 160.4837 15.27545040 80273
```

About [TLE](https://en.wikipedia.org/wiki/Two-line_element_set).

## LoRa communication payload onboard of STORK-1 mission. 

The CSUG designs a LoRa communication board and antenna for cubesats. The board and the antenna are hosted by the [SatRevolution's Stork mission](https://satrevolution.com/products/stork-mission/).
The board embeds one [Semtech SX1302 concentrator](https://www.semtech.com/products/wireless-rf/lora-gateways/sx1302) for communications on the 863-870 MHz band and one [Semtech SX1280 transceiver](https://www.semtech.com/products/wireless-rf/24-ghz-transceivers/sx1280) for communications on the 2400-2500 MHz band. The firmware of the two STM32 MCUs is developed with [RIOT OS](https://github.com/RIOT-OS/RIOT).


### Mission scenarios

Mission scenarios are described [here](./mission_scenario/README.md).

For mission scenario on 863-870 MHz band, the power consumption of the board at 3V3 is:
* 90 mA in standby,
* 100 mA while the SX1302 is started,
* 110 mA during a frame reception (RX) and
* 300 mA during a frame transmit (TX) at 27 dBm (RFPower).

### 868MHZ budget link

As the budget link depends on many factors(types of antenna on the ground, pointing accuracies, cubesat attitude, cubesat trajectory, ...), the following diagram is here to give a rough idea of the main characteristics (in terms of orbit and communication) of a ThingSat scenario on the 868MHz. One essential feature is that the cubesat is equipped with an attitude control system allowing it to point permanently towards the center of the Earth (i.e. Nadir). The calculation of the link budget allows us to hope for visibility windows of the cubesat of the order of 3 min for each passage. Because of its orbit (~525-km altitude, SSO), the cubesat will make 4 to 5 passes per day over the same area (the footprint being of the order of 1000km diameter). for the sake of simplicity, this diagram also assumes that the end-node and ground-station are in the orbit plane of the cubesat.

![868MHz Link Budget](./media/thingsat-linkbudget-total.png)

### Endpoints

The Thingsat board transmits LoRaWAN-compliant frames using the following parameters and credentials

<details>
<summary>Endpoints table</summary>

| Type     | Network   | DevAddr      | Codec           | Area | Key                                        |
| -------- | --------- | ------------ | --------------- | ---------- |----------------------------------------------- |
| `Beacon`  | CampusIoT |              | Beacon (inver IQ) | France, Svalbard, French Polynesia | Not relevant |
| `XBeacon` | CampusIoT | NORAD ID: 51087 | XBeacon (inver IQ) | France, Svalbard, French Polynesia | ED25519 PubKey: `760FB04B9B0EEE33E70A8BBD033020497A21B11456E623BF48949FC71665E785` |
| `DTUP`    | CampusIoT | `FC00AFF0`   | SOS             | France, Svalbard, French Polynesia | AES AppSKey: `cafebabe12345678cafebabe12345678` |
| `DTUP`    | CampusIoT | `FC00AFF1`   | Others than SOS | France, Svalbard, French Polynesia | AES AppSKey: `cafebabe12345678cafebabe12345678` |
| `DTUP`    | TTN       | `260BD663`   | All             | World (eu868 regions) | AES AppSKey: `cafebabe12345678cafebabe12345678` |
| `DTUP`    | [Requea](https://www.requea.com/)    | `FC006938`   | All             | France, Martinique, Guadeloupe, Réunion, Mayotte | AES AppSKey:  `cafebabe12345678cafebabe12345678` |
| `DTUP`    | [Actility](https://www.actility.com/iot-roaming-solution/)  | Coming soon  | All             | World  (with eu868 regional partners) | AES AppSKey: `cafebabe12345678cafebabe12345678` |
| `DTUP`    | [Orange LiveObject](https://liveobjects.orange-business.com/)    | `1e75d619`  | All | France (eu868) | AES AppSKey: `cf7d3ea8ee0a8eb388c1d566f084b74a` |
| `DTUP`    | [Objenious Spot](https://spot.objenious.com/)    | `0eb536bd`  | All | France (eu868) | AES AppSKey: `954740cb3165e12c970b4aeada5eddd8` |
| `DTUP`    | [Helium](https://explorer.helium.com/)    | `48000007`  | All | World (eu868 regions)  | AES AppSKey: `869A9F323B21370EEB38EC06D64F09DC` |

</details>

The CSV files of the endpoints is [here](./messages/endpoints.pub.csv).

> Frames payload can be decrypted with the AppSKey by ground receivers.

> Frames MIC can be checked by the Thingsat backend with the NwkSKey (which is kept secret) and the last fCntUp (Frame Counter).

> XBeacon frames are experimental beacons signed with an Elliptic Curve Cryptography (ECC) algorithm (ED25519). Signatures can be verified with convenient libs and tools such as [ed25519_applet](https://cyphr.me/ed25519_applet/ed.html).

### Payload and Beacon formats

[Draft](./messages)

### Dual-band antenna

The patch antenna enables communications on 863-870 MHz band and 2400-2500 MHz band. The polarization is circular. The substrat is [Roger RO3006](https://rogerscorp.com/).

[More details](antenna.md)

## Ground segment

Thingsat is registered on [TinyGS](https://tinygs.com/satellite/ThingSat)

### TinyGS ground stations
Search `CSUG` at https://tinygs.com/stations

Current gateways @ [CSUG](https://www.csug.fr/) are:
* [CSUG_01_Heltec868](https://tinygs.com/station/CSUG_02_Heltec868@1830594236)
* [CSUG_02_Heltec868](https://tinygs.com/station/CSUG_01_Heltec868@1830594236)

TinyGS 2G4 gateway:
* [ESP32 Vroom + Lambda80/E28 2.4GHz LoRa module](https://github.com/thingsat/tinygs_2g4station/blob/main/README.md)

## Datasets

Datasets will be hosted and referenced with DOI by [PerSCiDO](https://perscido.univ-grenoble-alpes.fr/).

## Partners
[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), [IMEP-LaHC](https://imep-lahc.grenoble-inp.fr/)), [Université Polynésie Française](https://www.upf.pf/fr), [Institut Polaire Paul Emile Victor (IPEV)](https://www.institut-polaire.fr/ipev-en/the-institute/), [SpaceAble](https://spaceable.org/), [Air Liquide](https://www.airliquide.com/fr), [Gorgy Timing](https://www.gorgy-timing.fr/), [Galatea](https://www.galatea.io/).

Special thanks to [ANS Innovation](https://www.ans-innovation.fr/), [Semtech](https://www.semtech.com/), [Chipselect](http://chipselect.fr/), [Farnell France](https://fr.farnell.com/), [ST Microelectronics](https://www.st.com/content/st_com/en.html), [Strataggem](https://www.strataggem.com/), [RIOTOS team](https://www.riot-os.org/), [CNES](https://cnes.fr/en), [RESA](http://www.resa-spatiales.com/?lang=en), [Synergie Concept](http://www.synergie-concept.fr/), [Nicomatic](https://www.nicomatic.com/), [Rogers Corp](https://rogerscorp.com/) for their help during the manufacturing of the payload.

Special thanks to LoRaWAN network operators: [Requea](https://www.requea.com/), [Actility](https://www.actility.com/), [Orange LiveObject](https://liveobjects.orange-business.com/).

## Frequencies
Frequencies are currently requested to [IARU](https://www.iaru.org/) and [ITU](https://www.itu.int/) for eu868 (867.5 and 869.525 MHz) and ism2400 bands (2425 and 2478 MHz).

## Gallery

Photo credits: [CSUG](https://www.csug.fr/), [SatRevolution](https://satrevolution.com/). SpaceX, JM Friedt.

### Board

The MCUs are :
* a [STM32F405RG](https://www.st.com/en/microcontrollers-microprocessors/stm32f405rg.html) for driving the SX1302+SX1250 gateway,
* and a [STM32L051K8](https://www.st.com/en/microcontrollers-microprocessors/stm32l051k8.html) for driving the SX1280 endpoint.

The SX1302+SX1259 and SX1280 circuitries are covered by EM shields on the flight model (FM) aboard Stork-1.

The reset button and the USB power supply connector has been removed before flight on the flight model (FM) aboard Stork-1.

![Thingsat board](./media/thingsat-annotated-fm-board.jpg)

### Dual-band antenna (868MHz, 2400MHz)

![Thingsat antenna](./media/thingsat-antenna.png)
![Thingsat antenna](./media/antenna_thingsat-01.jpg)
![Thingsat antenna](./media/antenna_thingsat-02.jpg)
![Thingsat antenna](./media/patch_thingsat_anechoic.jpg)

### Stork-1 Platform

![Thingsat integrated into Stork platform](./media/stork-1-in-case.png)
![Stork-1 mission (Jan. 2022)](./media/stork-1.png)
![Stork Alu Mockup ](./media/maquetteAluStork-1-01.jpg)

### Ground segment

![CCC](../ccc/images/mcs-01.png)

## TinyGS 2G4 gateway

![TinyGS 2G4](https://raw.githubusercontent.com/thingsat/tinygs_2g4station/main/images/gateway_tinygs_2g4-d-all_components.jpg)

## Launcher SpaceX Falcon 9 Block 5 Transporter 3 SSO Ride
![SpaceX Falcon 9 Block 5 Transporter 3 SSO Ride](https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/launcher_images/falcon_9_block__image_20210506060831.jpg)


## Glacier Field Test @ [Forskningsstationen Jean Corbel, Svalbard](https://goo.gl/maps/eUwj2Tnxwhi2vkkt9) with IPEV and Femto ST

![IPEVSvalbardFieldTest](./media/IPEVSvalbardFieldTest.jpg)


