# Thingsat Dual-band Antenna @ STORK-1

Author: Tan-Phu Vuong, CROMA, Grenoble-INP

The patch antenna enables communications on 863-870 MHz band and 2400-2500 MHz band.

The polarization is circular.

The substrat is [Roger RO3006](https://rogerscorp.com/).

![Thingsat antenna](media/thingsat-ant.png)
![Thingsat antenna](media/thingsat-antenna-gain.png)
![Thingsat antenna](media/thingsat-diag-2400.png)
![Thingsat antenna](media/thingsat-diag-868.png)
![Thingsat antenna](./media/thingsat-antenna.png)
![Thingsat antenna](./media/antenna_thingsat-01.jpg)
![Thingsat antenna](./media/antenna_thingsat-02.jpg)
![Thingsat antenna](./media/patch_thingsat_anechoic.jpg)
![](media/antenna-stork1-01.jpg)
![](media/antenna-stork1-02.jpg)

