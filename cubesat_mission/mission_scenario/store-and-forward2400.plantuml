@startuml "store-and-forward2400"

' =======================================
' Thingsat :: Mission Scenario
' (c) CSUG, UGA, 2020-2022
' =======================================

' =======================================
' Common procedures and functions
' =======================================

' ---------------------------------------
!procedure $introduction($scenario)

skinparam sequenceArrowThickness 2
skinparam roundcorner 10
skinparam sequenceParticipant underline
header Thingsat Mission Scenario :: $scenario   - %date()
footer (c) UGA, CSUG, 2020-2022 - Page %page% of %lastpage%  - %date()

title Thingsat Mission Scenario :: $scenario

participant "LoRa Ground Networks" as LoRa
participant Payload
participant OBC
participant "Ground Segment" as GroundSegment

note over LoRa: LoRa Ground Networks are\nground endpoints,\nand ground base stations\nbelonging or not to partners

!endprocedure

' ---------------------------------------
!procedure $begin($cmdline)
== Startup ==

Payload<-OBC : Power On & Reset

Payload->OBC : GetCommandToRunRequest
Payload<--OBC : GetCommandToRunResponse(command="$cmdline")

%invoke_procedure("$getenv", "First")

!endprocedure


' ---------------------------------------
!procedure $readfile($filename)

group Read file
    "$filename"

    Payload->OBC : OpenFileRequest(filename="$filename", mode=READ)
    Payload<--OBC : OpenFileResponse
    loop
        Payload->OBC : ReadFileChunkRequest(s)
        Payload<--OBC : ReadFileChunkResponse(s)
    end
    Payload->OBC : CloseFileRequest
    Payload<--OBC : CloseFileResponse

end

!endprocedure

' ---------------------------------------
!procedure $writefile($filename)

group Write file "$filename"

    Payload->OBC : OpenFileRequest(filename="$filename", mode=WRITE)
    Payload<--OBC : OpenFileResponse
    loop
        Payload->OBC : WriteFileChunkRequest(s)
        Payload<--OBC : WriteFileChunkResponse(s)
    end
    Payload->OBC : CloseFileRequest
    Payload<--OBC : CloseFileResponse

end

!endprocedure

' ---------------------------------------
!procedure $getenv($txt)
group Get env.

    note over Payload: $txt ,\nthe Payload records the env. params\n(timestamp, gps, attitude, battery level ... \nand remaining time before shutdown).

    Payload->OBC : GetEnvRequest
    Payload<--OBC : GetEnvResponse

end
!endprocedure

' ---------------------------------------
!procedure $beginning_scenario($title)

... wait seconds before starting the scenario\nat the expected timestamp ...

== Begining of the $title scenario ==

%invoke_procedure("$getenv", "Before each scenario")

group SX1280 modem started

    note over of Payload: Payload starts the SX1280 modem\nfor receiving LoRa frames\nfrom the ground endpoints\nbelonging or not to partners.

    !endprocedure

    ' ---------------------------------------
    !procedure $end_scenario($title,$filename)
end
%invoke_procedure("$getenv", "After each scenario")

group Write file "$filename"

    Payload->OBC : OpenFileRequest(filename="$filename", mode=WRITE)
    Payload<--OBC : OpenFileResponse
    loop
        Payload->OBC : WriteFileChunkRequest(s)
        Payload<--OBC : WriteFileChunkResponse(s)
    end
    Payload->OBC : CloseFileRequest
    Payload<--OBC : CloseFileResponse

    note over Payload, OBC: .RES file size is less than 2 KBytes

end

== End of the $title scenario ==

!endprocedure

' ---------------------------------------
!procedure $diagnosis($filename)
== Start of diagnosis ==

%invoke_procedure("$getenv", "For diagnosis,")

group Write file "$filename"

    Payload->OBC : OpenFileRequest(filename="$filename", mode=WRITE)
    Payload<--OBC : OpenFileResponse
    loop
        Payload->OBC : WriteFileChunkRequest(s)
        Payload<--OBC : WriteFileChunkResponse(s)
    end
    Payload->OBC : CloseFileRequest
    Payload<--OBC : CloseFileResponse

    note over Payload, OBC: .DIA file size is less than 0.2 Kbytes
end
== End of diagnosis ==
!endprocedure

' ---------------------------------------
!procedure $shutdown()

== Shutdown ==

Payload->OBC : ShutdownRequest

Payload<--OBC : Shutdown

!endprocedure


' =======================================
' Variables
' =======================================

' TODO

' =======================================
' Scenario
' =======================================

%invoke_procedure("$introduction", "Store-and-Forward 2400")

OBC<-GroundSegment : Send file (filename="0015.EXP")
note left: .EXP file size is less than 2 Kbytes.

OBC<-GroundSegment : Send mission plan for Payload

%invoke_procedure("$begin", "EXP 0015")

%invoke_procedure("$readfile", "0015.EXP")

note over Payload: A experiment is a sequence of 1 to 32 scenarios

' ---------------------------------------

%invoke_procedure("$beginning_scenario", "STORETMP2400")

... several seconds ...

LoRa->Payload : LoRa frame (from a partner's network)\nDuration : 0.05 to 2 seconds

... several seconds ...

LoRa->Payload : LoRa frame (from a partner's network)\nDuration : 0.05 to 2 seconds

... several seconds ...

LoRa->Payload : LoRa frame (from a partner's network)\nDuration : 0.05 to 2 seconds

... several seconds ...

LoRa->Payload : LoRa frame\nDuration : 0.05 to 2 seconds

note right of Payload: Payload filter frames and\nstore those belonging to partners\ninto several files .TMP.

group Write file "00150000.TMP"
    Payload->OBC : OpenFileRequest(filename="00150000.TMP", mode=WRITE)
    Payload<--OBC : OpenFileResponse
    loop
    Payload->OBC : WriteFileChunkRequest(s)
    Payload<--OBC : WriteFileChunkResponse(s)
    end
    Payload->OBC : CloseFileRequest
    Payload<--OBC : CloseFileResponse
    end


    ... several seconds ...

    LoRa->Payload : LoRa frame (from a partner's network)\nDuration : 0.05 to 2 seconds

    ... several seconds ...

    LoRa->Payload : LoRa frame (from a partner's network)\nDuration : 0.05 to 2 seconds

    ... several seconds ...

    LoRa->Payload : LoRa frame\nDuration : 0.05 to 2 seconds

    ... several seconds ...

    group Write file "00150001.TMP"
    Payload->OBC : OpenFileRequest(filename="00150001.TMP", mode=WRITE)
    Payload<--OBC : OpenFileResponse
    loop
        Payload->OBC : WriteFileChunkRequest(s)
        Payload<--OBC : WriteFileChunkResponse(s)
    end
    Payload->OBC : CloseFileRequest
    Payload<--OBC : CloseFileResponse
end

' -----------------------------------

%invoke_procedure("$end_scenario", "STORETMP2400", "001500.RES")

' -----------------------------------

%invoke_procedure("$beginning_scenario", "FORWARD2400")

%invoke_procedure("$readfile", "00150000.TMP")

... several seconds ...

LoRa<-Payload : LoRa frame (to partner's network)\nDuration : 0.05 to 2 seconds

note over LoRa, Payload: A received frame can be sent as it\nor encapsulated into the payload of a REPEATED frame\nwith the DevAddr of one of the Thingsat's endpoints.

... several seconds ...

LoRa<-Payload : LoRa frame (to partner's network)\nDuration : 0.05 to 2 seconds

... several seconds ...

LoRa<-Payload : LoRa frame (to partner's network)\nDuration : 0.05 to 2 seconds

... several seconds ...

note right of Payload: Payload filter frames and\nstore those belonging to partners\ninto several files .TMP.

%invoke_procedure("$readfile", "00150001.TMP")


LoRa<-Payload : LoRa frame (to partner's network)\nDuration : 0.05 to 2 seconds

... several seconds ...

LoRa<-Payload : LoRa frame (to partner's network)\nDuration : 0.05 to 2 seconds

... several seconds ...

group Read file "00150002.TMP"
    Payload->OBC : OpenFileRequest(filename="00150002.TMP", mode=WRITE)
    Payload<--OBC: OpenFileResponse(error=FILE_NOT_EXIST)
end
' -----------------------------------

%invoke_procedure("$end_scenario", "FORWARD2400", "001501.RES")

' -----------------------------------

%invoke_procedure("$diagnosis", "0015.DIA")

' -----------------------------------

%invoke_procedure("$shutdown")

== Downlink ==

OBC->GroundSegment : Send file with filename="001500.RES"
OBC->GroundSegment : Send file with filename="001501.RES"
OBC->GroundSegment : Send file with filename="0015.DIA"

OBC<-GroundSegment : Remove files (filename="*.TMP")
note left: .TMP files can be removed after the experiment\nin order to reclain disk space.
@enduml