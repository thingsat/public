# Thingsat Missions :: Workflows

The workflows of the mission scenarios are described with UML2.0 sequence diagrams.

Participants are (from left to right):
* The LoRa and LoRaWAN ground networks (ie LoRaWAN terrestrial stations, LoRaWAN terrestrial endpoints, SatIoT endpoints, SatIoT ground stations, [TinyGS](https://tinygs.com/)'s ground stations)
* The [Thingsat payload](../README.md#board) (with SX1302 concentrator and SX1280 tranceiver)
* The Onboard Computer (abbrev. OBC) of the Platform ([SatRev's STORK-1](../README.md#stork-1-platform))
* The Ground Segment of the Platform (SatRev's GS)

## Mission Scenario

On 863-870 MHz declared channels

* RX868 [plantuml](./rx868.plantuml), [svg](./rx868.svg)
* STORE868 [plantuml](./store868.plantuml), [svg](./store868.svg)
* TX868 [plantuml](./tx868.plantuml), [svg](./tx868.svg)
* REPEAT868 [plantuml](./repeat868.plantuml), [svg](./repeat868.svg)
* STORETMP868 + FORWARD868 [plantuml](./store-and-forward868.plantuml), [svg](./store-and-forward868.svg)

On 2400-2450 and 2450-2483 MHz declared channels

* RX2400 [plantuml](./rx2400.plantuml), [svg](./rx2400.svg)
* STORE2400 [plantuml](./store2400.plantuml), [svg](./store2400.svg)
* TX2400 [plantuml](./tx2400.plantuml), [svg](./tx2400.svg)
* REPEAT2400 [plantuml](./repeat2400.plantuml), [svg](./repeat2400.svg)
* STORETMP2400 + FORWARD2400 [plantuml](./store-and-forward2400.plantuml), [svg](./store-and-forward2400.svg)


### RX868 Mission Scenario

<details>
<summary>RX868 sequence diagram</summary>
![RX868](./rx868.svg)
</details>

### STORE868 Mission Scenario

<details>
<summary>STORE868 sequence diagram</summary>
![STORE868](./store868.svg)
</details>

### TX868 Mission Scenario

<details>
<summary>TX868 sequence diagram</summary>
![TX868](./tx868.svg)
</details>

### REPEAT868 Mission Scenario

<details>
<summary>REPEAT868 sequence diagram</summary>
![REPEAT868](./repeat868.svg)
</details>

### STORETMP868 + FORWARD868 Mission Scenario
<details>
<summary>STORETMP868 + FORWARD868 sequence diagram</summary>
![STORETMP868 + FORWARD868](./store-and-forward868.svg)
</details>

### RX2400 Mission Scenario

<details>
<summary>RX2400 sequence diagram</summary>
![RX2400](./rx2400.svg)
</details>

### STORE2400 Mission Scenario

<details>
<summary>STORE2400 sequence diagram</summary>
![STORE2400](./store2400.svg)
</details>

### TX2400 Mission Scenario

<details>
<summary>TX2400 sequence diagram</summary>
![TX2400](./tx2400.svg)
</details>

### REPEAT2400 Mission Scenario

<details>
<summary>REPEAT2400 sequence diagram</summary>
![REPEAT2400](./repeat2400.svg)
</details>

### STORETMP2400 + FORWARD2400 Mission Scenario

<details>
<summary>STORETMP2400 + FORWARD2400 sequence diagram</summary>
![STORETMP2400 + FORWARD2400](./store-and-forward2400.svg)
</details>
