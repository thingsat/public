## PlantUML syntax

Sequence diagrams are describing with the [PlantUML syntax](https://plantuml.com/fr/sequence-diagram).

## Editor

[Visual Studio extension](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)

Live editor
* https://www.panjianning.com/puml
* https://github.com/panjianning/plantuml-live-editor

Choose SVG for export

## Docker 

```bash
docker run -t --rm  -v `pwd`:/var/docs plantuml/plantuml -h

FILES=$(for f in *.plantuml; do echo /var/docs/$f; done)
docker run -t --rm  -v `pwd`:/var/docs plantuml/plantuml -nbthread 8 -tsvg -o /var/docs $FILES
docker run -t --rm  -v `pwd`:/var/docs plantuml/plantuml -nbthread 8 -tpng -o /var/docs $FILES
```

## CI

[PlantUML and GitLab](https://docs.gitlab.com/ee/administration/integration/plantuml.html)
