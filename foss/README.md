# Thingsat :: Free and Open Source Software (FOSS)

## Flight segment

The firmware of the Thingsat's board is developed in C with RIOT OS. The release of main modules (drivers, cpu, mission engine ...) in FOSS is coming soon.

The contributions are listed in active branches of https://github.com/thingsat/RIOT/branches : 

- :white_check_mark: (Done) 19 mai 2022 : RIOT [sx1280 driver](https://github.com/RIOT-OS/RIOT/pull/18033) and tests
- :x: (coming soon) RIOT sx1302 driver + LoRa packet forwarder + store-carry-and-forward algorithm 
- :x: (coming soon) stts751 temperature driver 
- :x: (coming soon) libcsp over CAN 
- :x: (coming soon) SUIT (Software Update for Internet of Thing) over libcsp over CAN 
- :x: (coming soon) SUIT over fatfs 

## Ground station

* [TinyGS 2G4](https://github.com/thingsat/tinygs_2g4station/tree/main/Firmware)


## Balloon endpoint

* [LoRaWAN Field Test Device](https://github.com/CampusIoT/orbimote/tree/master/field_test_device#readme)

## Ground endpoint

* [LoRaWAN Field Test Device + TLE](https://github.com/CampusIoT/orbimote/tree/master/field_test_device#readme)
* TLE parser and predict lib (coming soon)

## Ground segment

The Thingsat's ground control is a micro-service architecture deployed on cloud using Docker and Kubernetes. The mission planner microservice is developed with NodeRED and VTS. The release in FOSS is coming soon. The integration of LoRaWAN public and private networks is developed with NodeRED. The LoRaWAN Network Server is Chirpstack.
