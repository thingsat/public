# Antennas for ground stations and ground endpoints


## 868 MHz

| Brand | Ref | Band | Gain | Polarization | Aperture | Size | Price |
| ----- | --- | ---- | ---- | ------------ | -------- | ---- | ----- |
| RFThings | [RT-CP3-BL RFThings Edition](https://rfthings.com.vn/product/rt-cp3-bl-rfthings-edition/)  | 868 MHz |   | Circular  | 110° | 73 x 73 x 15 (mm) | 17$ |
|   |   |   |   |   |   |  | |


