# Thingsat :: Free and Open Source Hardware

Coming soon

* Cubesat LoRa Communication Board (License: [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/))
* Dual-Band Cubesat Antenna (863-870 MHz & 2400-2500 MHz)
* [TinyGS Ground Stations](https://github.com/thingsat/tinygs_2g4station/tree/main/Board_Tinysgs_2.4GHz_V1) (License: [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/))
