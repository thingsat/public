# Thingsat :: Ground endpoints

The Thingsat mission experiments LoRa communications on 2 RF bands : 868-870 MHz and 2400-2500 MHz

## 868-870 MHz endpoints
* [iM880a DS75LX](https://github.com/CampusIoT/tutorial/blob/master/im880a/im880a-ds75lx.md)
* [Wio Terminal](https://github.com/CampusIoT/tutorial/tree/master/wioterminal) + [LoRa E5 Grove](https://wiki.seeedstudio.com/Grove_LoRa_E5_New_Version/)
* [Seeedstudio LoRa E5 Dev and Mini](https://wiki.seeedstudio.com/LoRa_E5_Dev_Board/)
* Heltec32 LoRa
* STM32 Nucleo board + MBed SX127x shields
* [Dragino LSN50-v2](https://www.dragino.com/products/lora-lorawan-end-node/item/155-lsn50-v2.html) (+ external probes)

## 2400-2500 MHz endpoints
* [TinyGS 2G4](./https://github.com/thingsat/tinygs_2g4station/blob/main/README.md)
* [Wio Terminal](https://github.com/CampusIoT/tutorial/tree/master/wioterminal) + Mikrobus adapters for SX1280 modules

## Firmware
* Arduino
* RIOT OS
* MicroPython

## Media
![IMST](https://raw.githubusercontent.com/CampusIoT/tutorial/master/im880a/figs/im880a-ds75lx-top.jpg)
![TinyGS 2G4](https://raw.githubusercontent.com/thingsat/tinygs_2g4station/main/images/gateway_tinygs_2g4-d-all_components.jpg)
TinyGS 2G4 PCB with ESP32 Vroom, Grove boards ([Grove Thumb Joystick](https://wiki.seeedstudio.com/Grove-Thumb_Joystick/), [Grove  LSM6DS3 Accelerometer Gyroscope](https://wiki.seeedstudio.com/Grove-6-Axis_AccelerometerAndGyroscope/), [Grove GPS](https://wiki.seeedstudio.com/Grove-GPS/)) and Lamdba80 and E28 module.


