# Thingsat :: LoRa/LR-FHSS and SatIoT benchmarks from CSUT Nimph cubesat (New mission expected on Q1 2026)

[ThingSat project](https://www.csug.fr/main-menu/projects/thingsat-internet-of-isolated-objects-by-satellite--929824.kjsp) developed by [CSUG](https://www.csug.fr/en/) and its [partners](#partners) aims to create a CubeSat payload dedicated to design and to benchmark long-distance communication protocols with low-power consumption LoRa modulation for Bidirectional Satellite IoT services (SatIoT) and Low Power Global Area Networks (LPGAN) ([publications and presentations](../publications.md)).

## Keywords

LoRa, LR-FHSS, LoRaWAN, Bidirectional SatIoT, LPGAN, Secure Firmware Update over the Space, CubeSat, New Space, Post-Quantum Cryptography, TinyML

## Aims

* Communications
  * Benchmarking LoRa links over very long distance (> 500 kms) for several frequency bands (868MHz, 2.4GHz) including immunity to Doppler effect.
  * Studying frames of ground LoRaWAN endpoints from the space.
  * Receiving frames sent by cubesats using ground terestrial LoRaWAN stations.
  * Designing and evaluating  LoRa [delay-tolerant networks](https://en.wikipedia.org/wiki/Delay-tolerant_networking) algorithms (store-and-forward principle).

* Applications
  * Synchronizing ground LoRaWAN endpoints clocks and constellation orbits parameters (ie TLE) from the space.
  * Repeating LoRa frames of ground LoRaWAN trackers (emergency usecase).
  * Monitoring with sensors in real field cases : artic glaciers, Helium tank containers, fish farms, young turtle tracking ...
  * Ground LoRaWAN network quality cartography from space point-of-view (uplink and downlink).
  * [Space Situational Awaraness (SSA)](https://en.wikipedia.org/wiki/Space_Situational_Awareness_Programme) : Space debris beacons and tracking.
  * EWSS ([Emergency Warning Satellite Service](../cubesat_mission_2/ewss_sar.md)) compliant-messages broadcasting over LoRaWAN (LoRa and LR-FHSS)
  * SAR ([Search-and-Rescue)](../cubesat_mission_2/ewss_sar.md)) compliant-messages relaying over LoRaWAN (LoRa and LR-FHSS)
  * AIS ([Automatic Identification System )](../cubesat_mission_2/ais.md)) compliant-messages relaying over LoRaWAN (LoRa and LR-FHSS)
  * APRS ([Automatic Packet Reporting System )](../cubesat_mission_2/aprs.md)) compliant-messages relaying over LoRaWAN (LoRa and LR-FHSS)


Moreover, we tests new security features for firmware updates over the space using [SUIT](https://datatracker.ietf.org/wg/suit/about/), [FemtoContainers](https://github.com/future-proof-iot/Femto-Container_tutorials) and TinyML on MEMS signals.

![Delay Tolerant Network](../cubesat_mission_2/media/thingsat-dtn.png)

## Test fields

* Glaciers monitoring (French Alps & Ny Alesund, Svalbard)
* Fish farming, Young turtle tracking (French Polynesia)
* Helium container maritime shipping (Oceans)
* [Micro-weather forecasting for predicting Malaria epidemics](https://i2hm.github.io/) (Sub-saharian countries)
* Secure clock synchronization (Europe, Africa, French Polynesia, Oceans)

## Launch

Expected date: [Q1 2026](https://db.satnogs.org/search/?q=Nimph).

Follow Thingsat on [CSUG Linkedin](https://www.linkedin.com/company/csug) and [CSUT Linkedin](https://www.linkedin.com/company/csut).

## Orbit

Low Earth Orbit SSO. Altitude ~500 kms.

## LoRa/LR-FHSS communication payload onboard of [CSUT Nimph](https://www.csut.cnrs.fr/project/nanosatellite-to-investigate-microwave-photonics-hardware/) mission. 

The CSUG designs a LoRa communication board and antenna for cubesats. The board and the antenna are hosted by the [CSUT Nimph mission](https://www.csut.cnrs.fr/project/nanosatellite-to-investigate-microwave-photonics-hardware/).
The board embeds one [Semtech SX1303 concentrator](https://www.semtech.com/products/wireless-rf/lora-gateways/sx1303) for communications on the 863-870 MHz band.

The firmware of the STM32 MCU is developed with [RIOT OS](https://github.com/RIOT-OS/RIOT). The firmware is partially available in [open source](https://github.com/thingsat/riot_modules). The firmware can be updated securelly with [SUIT](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8725488). The firmware will support [Femto Containers](https://dl.acm.org/doi/pdf/10.1145/3528535.3565242) for custom mission scenario compiled in [rBPF](https://hal.science/hal-03019639v1) bytecode. Mission files and rBPF bytecode files are signed using SUIT.

### Mission scenarios

The mission scenarios are described [here](../cubesat_mission_2/mission_scenario/README.md).

The description files (experiments and results) are defined [here](../cubesat_mission_2/descriptors/README.md).

### Secure communications

Several cryptographic algorithms will be involved for securing the communications and for signing the firmware to update, and for signing the mission descriptor files and result files.

* Symetric Cryptography: AES128 (default in LoRaWAN), AES192, AES256
* Asymetric Cryptography: [Elliptic-curve cryptography (ECC)](https://github.com/thingsat/ecc_mcu_benchmarks/blob/main/README.md) such as ED25519 (default in SUIT)
* Post-Quantum Cryptography: [NIST candidates](https://csrc.nist.gov/projects/post-quantum-cryptography/post-quantum-cryptography-standardization) such as [Kyber](https://pq-crystals.org/kyber/)

## Power consumption (extrapolated from [Stork Board](../cubesat_mission_2))

For mission scenario on 863-870 MHz band, the power consumption of the board at 5V0 is:
* 28 mA (peak 28mA at boot) in standby,
* 55 mA while the SX1303 is started,
* 70 mA during a frame reception (RX),
* 84 mA (peak 106 mA) during a frame transmit (TX) at 12 dBm (RFPower)
* 118 mA (peak 161 mA) during a frame transmit (TX) at 20 dBm (RFPower)
* 214 mA (peak 320 mA) during a frame transmit (TX) at 27 dBm (RFPower)
* 66 mA (peak 83 mA) during a frame transmit (TX) at 0 dBm (RFPower)

### 868MHZ budget link

As the budget link depends on many factors(types of antenna on the ground, pointing accuracies, cubesat attitude, cubesat trajectory, ...), the following diagram is here to give a rough idea of the main characteristics (in terms of orbit and communication) of a ThingSat scenario on the 868MHz. One essential feature is that the cubesat is equipped with an attitude control system allowing it to point permanently towards the center of the Earth (i.e. Nadir). The calculation of the link budget allows us to hope for visibility windows of the cubesat of the order of 3 min for each passage. Because of its orbit (~525-km altitude, SSO), the cubesat will make 4 to 5 passes per day over the same area (the footprint being of the order of 1000km diameter). for the sake of simplicity, this diagram also assumes that the end-node and ground-station are in the orbit plane of the cubesat.

![868MHz Link Budget](../cubesat_mission_2/media/thingsat-linkbudget-total.png)

### Endpoints

The Thingsat board transmits [LoRaWAN-compliant frames](https://lora-alliance.org/resource_hub/lorawan-104-specification-package/) using the following parameters and credentials

<details>
<summary>Endpoints table</summary>

| Type     | Network   | DevAddr      | Codec           | Area | Key                                        |
| -------- | --------- | ------------ | --------------- | ---------- |----------------------------------------------- |


</details>

The CSV files of the endpoints and keys is [here](../cubesat_mission_2/messages/endpoints.pub.csv).

> Frames payload can be decrypted with the AppSKey by ground receivers.

> Frames MIC can be checked by the Thingsat backend with the NwkSKey (which is kept secret) and the last fCntUp (Frame Counter).

> XBeacon frames are experimental beacons signed with an Elliptic Curve Cryptography (ECC) algorithm (ED25519). An ED25519 benchmark on MCU is available [here](https://github.com/thingsat/ecc_mcu_benchmarks). Signatures can be verified with convenient libs and tools such as [ed25519_applet](https://cyphr.me/ed25519_applet/ed.html). 

### Payload and Beacon formats

[Draft](../cubesat_mission_2/messages)

### Board

The MCU is a [STM32F405RG](https://www.st.com/en/microcontrollers-microprocessors/stm32f405rg.html) (192KB RAM, 1024 KB FlashRAM) for driving the [SX1303](https://www.semtech.fr/products/wireless-rf/lora-core/sx1303)+[SX1250](https://www.semtech.fr/products/wireless-rf/lora-core/sx1250) gateway and a [SX1262](https://www.semtech.fr/products/wireless-rf/lora-connect/sx1262) transceiver (for [LBT](https://ieeexplore.ieee.org/document/8935847), spectral scan and [LR-FHSS TX](https://arxiv.org/pdf/2010.00491.pdf) scenarios).

The Hosted Payload MCU is connected to the Nimph OBC ([Steel Ninano](https://satsearch.co/products/steel-electronique-ninano-on-board-computer-for-nanosatellites)) thought I2C dedicated line in the [SEED](https://cnes.fr/projets/nanolab-academy) PC104 connector.

The [SX1303](https://www.semtech.fr/products/wireless-rf/lora-core/sx1303)+[SX1250](https://www.semtech.fr/products/wireless-rf/lora-core/sx1250)+[SKY66420-11](https://www.skyworksinc.com/en/Products/Front-end-Modules/SKY66420-11)+[PE4259](https://www.psemi.com/pdf/datasheets/pe4259ds.pdf)+SX1262 circuitries are covered by EM shields on the flight model (FM) aboard Stork/SOWA platform.

The MEMS are [ST A3G4250D](https://www.st.com/en/mems-and-sensors/a3g4250d.html) (gyroscope), [ST LSM303AGRTR](https://www.st.com/en/mems-and-sensors/lsm303agr.html) (accelerometer/magnetometer) and 4x [ST STTS751](https://www.st.com/en/mems-and-sensors/stts751.html) (temperature).

The on-board storage is managed into a [ST M24C01-FDW](https://www.st.com/resource/en/datasheet/m24c01-w.pdf) 1Mbit external EEPROM.

The power lines are protected from [latch-up](https://en.wikipedia.org/wiki/Latch-up) and monitored by four [MAX4773](https://www.analog.com/media/en/technical-documentation/data-sheets/MAX4772-MAX4773.pdf) LCL (Latch Up Current Limiter).

The I2C Grove connector enables to plug a GNSS module (I2C mode) for high-altitude testing aboard [stratospheric helium balloons](../balloons/). 

The side connector enables the diagnosis and the flashing of the FM board after integration and environmental tests. 

The flight model (FM) platform is not equipped with the reset button and the UART grove connector.

### 868 MHz planar antenna

The patch antenna enables communications on 863-870 MHz band. The polarization is circular. The substrat is [Roger](https://rogerscorp.com/) [RO4360](https://www.rogerscorp.com/-/media/project/rogerscorp/documents/advanced-electronics-solutions/english/data-sheets/ro4360g2-high-frequency-laminates-data-sheet.pdf).

More about the antenna (coming soon)

## Datasets

Datasets will be hosted and referenced with DOI by [PerSCiDO](https://perscido.univ-grenoble-alpes.fr/).

## Partners

[Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr/) ([CSUG](https://www.csug.fr/), [OSUG](https://www.osug.fr/), [LIG](https://www.liglab.fr/), CROMA ([IMEP-LaHC](https://imep-lahc.grenoble-inp.fr/)), Carnot LSI, CSUT, CNES Nanolab Academy and AMI CMA COMETES.

Previous missions were supported by : [Persyval-Lab](https://persyval-lab.org/en/platforms)), [CNRS INS2I](https://www.ins2i.cnrs.fr/fr), [Fondation UGA](https://fondation.univ-grenoble-alpes.fr/) [Requea](https://www.requea.com/), [SatRev](https://www.satrev.space/), [Université Polynésie Française](https://www.upf.pf/fr), [Institut Polaire Paul Emile Victor (IPEV)](https://www.institut-polaire.fr/ipev-en/the-institute/), [Air Liquide](https://www.airliquide.com/fr), [Galatea](https://www.galatea.io/).

## Thanks

Special thanks to [CEDMS](https://iut1.univ-grenoble-alpes.fr/medias/fichier/2020-geii-plateforme-cedms_1643280966346-pdf), [Carnot LSI](https://carnot-lsi.com/), [ANS Innovation](https://www.ans-innovation.fr/), [Semtech](https://www.semtech.com/), [Chipselect](http://chipselect.fr/), [Farnell France](https://fr.farnell.com/), [ST Microelectronics](https://www.st.com/content/st_com/en.html), [Strataggem](https://www.strataggem.com/), [RIOTOS team](https://www.riot-os.org/), [CNES](https://cnes.fr/en), [RESA](http://www.resa-spatiales.com/?lang=en), [Synergie Concept](http://www.synergie-concept.fr/), [Nicomatic](https://www.nicomatic.com/), [Rogers Corp](https://rogerscorp.com/) for their help during the manufacturing of the payload.

Special thanks to LoRaWAN network operators: [Actility](https://www.actility.com/), [Orange LiveObject](https://liveobjects.orange-business.com/).

Join the [LoRaWAN Over Satellites Task Force](https://lora-alliance.org/become-a-member/#working-groups-and-task-forces) at LoRa Alliance.

## Frequencies

Frequencies are currently requested to [ITU](https://www.itu.int/) for eu868 (867.5 MHz and 869.525 MHz).

## Gallery

Photo credits: [CSUG](https://www.csug.fr/), CSUT, JM Freidt

### Engineering model

![Thingsat PC104](./media/thingsat-nimph-03-xs.png)

![Thingsat PC104](./media/thingsat-nimph-01.jpg)

### Pedagogical model (for [EasySpace INISAT](https://www.easy-space.fr/kits-pedagogiques-satellites/))

![Thingsat INISAT](../cubesat_mission_2/media/inisat-obc+rak5146.png) 

### Pedagogical model (for CNES Nanolab Academy's SEED platform)

![Thingsat SEED mockup](media/seed_rak5146.jpg) 

### CSUT Nimph

![Logo](media/logo-csut-nimph.png)
![Nimph model](media/nimph_csut.png)

### Ground segment

![CCC](../ccc/images/mcs-01.png)

## Glacier Field Test @ [Forskningsstationen Jean Corbel, Svalbard](https://goo.gl/maps/eUwj2Tnxwhi2vkkt9) with IPEV and Femto ST

![IPEVSvalbardFieldTest](media/IPEVSvalbardFieldTest.jpg)
![Lorix @ Ny Alesund](media/lorix_nyalesund.jpg)

