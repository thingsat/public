# Thingsat :: Ground gateways

The Thingsat mission experiments LoRa communications on 2 RF bands : 868-870 MHz and 2400-2500 MHz

## 868-870 MHz gateways
* Mikrotik LoRa LAP8
* Kerlink iStation
* Strataggem Picogateway

## 2400-2500 MHz gateways
* Multitech MTCDT + MCard 2G4
* [TinyGS 2G4](https://github.com/thingsat/tinygs_2g4station/blob/main/README.md)


![TinyGS 2G4](https://raw.githubusercontent.com/thingsat/tinygs_2g4station/main/images/gateway_tinygs_2g4-d-all_components.jpg)
TinyGS 2G4 PCB with ESP32 Vroom, Grove boards ([Grove Thumb Joystick](https://wiki.seeedstudio.com/Grove-Thumb_Joystick/), [Grove  LSM6DS3 Accelerometer Gyroscope](https://wiki.seeedstudio.com/Grove-6-Axis_AccelerometerAndGyroscope/), [Grove GPS](https://wiki.seeedstudio.com/Grove-GPS/)) and Lamdba80 and E28 module.
