# Thingsat :: LoRa and SatIoT benchmarks from cubesat :: Publications & Presentations

* 2023
    * Toujours plus haut, Toujours plus loin ou Thingsat un véhicule de recherche pour l’expérimentation in-vivo ouverte de l’Internet des Objets par Satellite, [CONFERENCE DE RECHERCHE EN INFORMATIQUE CRI'2023](http://cri-info.cm/), Yaoundé, Cameroon.
    * [ThingSat : une passerelle LoRa spatiale pour objets connectés terrestres](https://c2i-2023.sciencesconf.org/resource/page/id/8), 9ème Colloque Interdisciplinaire en instrumentation Grenoble 2023.
* 2022
    * [IoT and Satellite](https://rsd-ecole.cnrs.fr/2022/program/), RESCOM Summer school Yenne 2022.
    * [Retours d’expérience sur le projet ThingSat (Cubesat LoRa)](https://www.irit.fr/journees-lpwan-2022/programme/), Journées LPWAN Toulouse 2022 (in french).
    * Olivier Alphand, Thingsat : a LoRa Gateway CubeSat (1st feedbacks), [Space-Terrestrial Internetworking Workshop](https://www.stintworkshops.org/), 10th IEEE International Conference on Wireless for Space and Extreme Environments (WISEE 2022) [video](https://www.youtube.com/playlist?list=PLXfFUirnQ-PQ6gXA4XE0TpHdpgjyGhnno)
* 2021
    * [Projet ThingSat](https://journees-lpwan-2021.limos.fr/progprev.html), Journées LPWAN Clermont-Ferrand 2021.
* 2019
    * [Cubesats: A low cost opportunity for IoT satellites](https://gnsw.sciencesconf.org/274061), Grenoble New Space Week 2019.
    * [Cubesats: a Low Cost Opportunity for IoT Satellites](http://lpwan.conf.citi-lab.fr/pres/didier.pdf), [Journées LPWAN Lyon 2019](http://lpwan.conf.citi-lab.fr/). July 11-12 2019 
    * [Cubesats: a low cost opportunity for IoT satellites](https://summit.riot-os.org/2019/blog/speakers/didier-donsez/), [RIOT Summit 2019](https://summit.riot-os.org/2019/), Helsinki, September 5-6, 2019.

