# Thingsat Mission (October 2024) :: Workflows

The workflows of the mission scenarios are described with UML2.0 sequence diagrams.

Participants are (from left to right):
* The LoRa and LoRaWAN ground networks including LoRaWAN terrestrial stations, LoRaWAN terrestrial endpoints, SatIoT endpoints, SatIoT ground stations, [TinyGS](https://tinygs.com/)'s ground stations.
* The [Thingsat Hosted Payload](../README.md#board) (with SX1303 concentrator)
* The Onboard Computer (abbrev. OBC) of the Platform ([SatRev's STORK](../README.md))
* The Ground Segment of the Platform (SatRev's GS)

The format of the files exchanged between the Ground Segment, the Onboard Computer and the Hosted Payload are described [here](../descriptors/README.md).

## Mission Scenario

On 863-870 MHz declared channels

* `RX868` [plantuml](./rx868.plantuml), [svg](./rx868.svg), [png](./rx868.png)
* `STORE868` [plantuml](./store868.plantuml), [svg](./store868.svg), [png](./store868.png)
* `TX868` [plantuml](./tx868.plantuml), [svg](./tx868.svg), [png](./tx868.png)
* `REPEAT868` [plantuml](./repeat868.plantuml), [svg](./repeat868.svg), [png](./repeat868.png)
* `STORE868` + `FORWARD868` (for delay-tolerant networks) [plantuml](./store-and-forward868.plantuml), [svg](./store-and-forward868.svg), [png](./store-and-forward868.png)
* `RANGING868` : coming soon
* `SPECTRALSCAN868` : coming soon
* `TINYML` + `TX868` : coming soon
* `FUOTS` (Firmware Update Over The Space) : coming soon

### `RX868` Mission Scenario

<details>
<summary>`RX868` sequence diagram</summary>
![RX868](./rx868.svg)
</details>

### `TX868` Mission Scenario

<details>
<summary>`TX868` sequence diagram</summary>
![TX868](./tx868.svg)
</details>

### `REPEAT868` Mission Scenario

<details>
<summary>`REPEAT868` sequence diagram</summary>
![REPEAT868](./repeat868.svg)
</details>

### `STORE868` Mission Scenario

<details>
<summary>`STORE868` sequence diagram</summary>
![STORE868](./store868.svg)
</details>

### `STORE868` + `FORWARD868` Mission Scenario

For delay-tolerant networks (DTN)
<details>
<summary>`STORE868` + `FORWARD868` sequence diagram</summary>
![STORE868 + FORWARD868](./store-and-forward868.svg)
</details>

### `RANGING868` Mission Scenario

This scenario enables to send RangingRequest to the ground stations and to answer to RangingRequest from the ground stations.

The diagram of `RANGING868` mission scenario is coming soon.

### `SPECTRALSCAN868` Mission Scenario

The diagram of `SPECTRALSCAN868` mission scenario is coming soon. Note: it is very similar to `RX868` Mission Scenario.

### `TINYML` + `TX868` Mission Scenario

This scenario enables to run TFLite models on the Thingsat MCU on input data provided by onboard MEMS or by the OBC throught files to read.

The diagram of `TINYML` + `TX868` mission scenario is coming soon. Note: it is very similar to `TX868` Mission Scenario.

### `FUOTS` (Firmware Update Over The Space) Mission Scenario

The diagram of `FUOTS` mission scenario is coming soon. The firmware update relies on the [RIOT implementation](https://api.riot-os.org/group__sys__suit.html) of [SUIT IETF RFC](https://datatracker.ietf.org/group/suit/about/).

