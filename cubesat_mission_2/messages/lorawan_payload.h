/*
 * Copyright (C) 2020-2023 Université Grenoble Alpes
 */

/*
 * Author : Didier Donsez, Université Grenoble Alpes
 */

/*
 * Thingsat Mission Scenarii :: Data structures
 */

/*
 * Types for the format of the payload into the LoRaWAN frames sent and received by the Thingsat payload
 */


#ifndef LORAWAN_PAYLOAD_H
#define LORAWAN_PAYLOAD_H

#include <stdint.h>
#include <stdbool.h>

/*
 * FPort definition for typing the payload format
 */

#define FPORT_MAC_COMMANDS 						(0)

// Sat to Ground : IQ=normal CRC=on
#define FPORT_SOS_MESSAGE_PAYLOAD				(1U)
#define FPORT_DIAG_MESSAGE_PAYLOAD				(2U)
#define FPORT_REPEATED_MESSAGE_PAYLOAD			(3U)
#define FPORT_TELEMETRY_MESSAGE_PAYLOAD			(4U)

// Sat to Ground : IQ=inverted No CRC
#define FPORT_DN_TEXT_MESSAGE_PAYLOAD			(9U)
#define FPORT_TLE_MESSAGE_PAYLOAD				(10U)
#define FPORT_ED25519_PUBKEY_MESSAGE_PAYLOAD	(11U)
#define FPORT_APP_CLOCK_MESSAGE_PAYLOAD			(202U)  // Application Layer Clock

// Ground to Sat IQ=inverted No CRC
#define FPORT_UP_TEXT_MESSAGE_PAYLOAD			(33U)
#define FPORT_FORWARDING_MESSAGE_PAYLOAD		(34U)
#define FPORT_ACTION_MESSAGE_PAYLOAD			(35U)
#define FPORT_TIMING_TEST_MESSAGE_PAYLOAD		(36U)

// Transport: Data Up Frame and Data Down Frame
#define FPORT_XOR_MESSAGE_PAYLOAD				(64U)
#define FPORT_XOR2_MESSAGE_PAYLOAD				(FPORT_XOR_MESSAGE_PAYLOAD+2U)
#define FPORT_XOR4_MESSAGE_PAYLOAD				(FPORT_XOR_MESSAGE_PAYLOAD+4U)
#define FPORT_XOR8_MESSAGE_PAYLOAD				(FPORT_XOR_MESSAGE_PAYLOAD+8U)
#define FPORT_XOR16_MESSAGE_PAYLOAD				(FPORT_XOR_MESSAGE_PAYLOAD+16U)
#define FPORT_XOR32_MESSAGE_PAYLOAD				(FPORT_XOR_MESSAGE_PAYLOAD+32U)

// (eXperimental) Used by the application server (ie the cubesat) to the end-device to set the endpoint clock
#define X_APP_CLOCK_CID_NoradInfoReq			(uint8_t)0xFC
// (eXperimental) Used by the application server (ie the cubesat) to the end-device to set the endpoint clock
#define X_APP_CLOCK_CID_PackageVersionInfo		(uint8_t)0xFD
// (eXperimental) Used by the application server (ie the cubesat) to the end-device to set the endpoint clock
#define X_APP_CLOCK_CID_AppTimeSetReq			(uint8_t)0xFE

// TODO sizeof(RepeaterMessagePayload_t) should not exceed 251.
#define REPEATED_FRAME_MAX_LEN					(251U-6U)

/**
 * @brief Up Text Message Payload
 * The payload size should not be excced 51 bytes
 * Transport: Data Frame Dn
 */
struct __attribute__((__packed__)) UpTextMessagePayload
{
	uint16_t delay_before_forwarding; // * 10 seconds
	uint8_t  rfu : 7; // Reserved for future use
	uint8_t  algo : 1; // AES or ED25519 for MIC
	/*
	 * @brief message should contain the emitter id, the receiver id and the message separated by semi-colon.
	 * For instance "f12a34;d56b78;Hello from Grenoble, France !" or "f12a34;f*;Bonne annee 2022 !"
	 */
	char message[51-3];
};

typedef struct UpTextMessagePayload UpTextMessagePayload_t;


/**
 * @brief Dn Text Message Payload
 * The payload size should not be excced 51 bytes
 * Transport: Data Frame Up
 */
struct __attribute__((__packed__)) DnTextMessagePayload
{
	uint32_t noradnumber : 24;
	uint8_t esp;
	/*
	 * @brief message should contain the emitter id, the receiver id and the message separated by semi-colon.
	 * For instance "f12a34;d56b78;Hello from Grenoble, France !" or "f12a34;f*;Bonne annee 2022 !"
	 */
	char message[51-4];
};

typedef struct DnTextMessagePayload DnTextMessagePayload_t;


/**
 * @brief SOS Message Payload
 * Transport: Data Frame Up
 */
struct __attribute__((__packed__)) SosMessagePayload
{
    /*
     * @brief Firmware version
     */
    uint32_t  firmware_version;

    /*
     * @brief Slot ID
     */
    uint8_t  slot_id; // 0 or 1

    /*
     * @brief Board uptime (in seconds)
     */
    uint32_t  uptime : 24;

    /*
     * @brief Error status
     */
	uint8_t error_status;

    /*
     * @brief Temperature (in ??)
     */
    uint8_t  temperature;

    /*
     * @brief Roll (X) angular rate in mdps (from gyroscope)
     */
    int32_t  roll;

    /*
     * @brief Pitch (Y) angular rate in mdps (from gyroscope)
     * @TODO change for int16_t
     */
    int32_t  pitch;

    /*
     * @brief Yaw (Z) angular rate in mdps (from gyroscope)
     * @TODO change for int16_t
     */
    int32_t  yaw;

    /*
     * @brief SOS endpoint index of the last executed action
     */
    uint8_t last_action_endpoint_index;

    /*
     * @brief FCnt Down of the last executed action
     */
    uint16_t last_action_fcnt;


    /*
     * Part of the current ResultStat
     */

	/*
	 * @brief Tx counter
	 */
	uint16_t tx_cnt;

	/*
	 * @brief Tx abort counter
	 */
	uint16_t tx_abort_cnt;

	/*
	 * @brief Rx counter with a valid CRC
	 */
	uint16_t rx_ok_cnt;

	/*
	 * @brief Rx counter with no CRC
	 */
	uint16_t rx_no_crc_cnt;

	/*
	 * @brief Rx counter of frames with bad CRC
	 */
	uint16_t rx_bad_crc_cnt;

	/*
	 * @brief Rx counter of messages for endpoints described into the ExpConfig
	 */
	uint16_t rx_endpoint_cnt;

	/*
	 * @brief Rx counter with JREQ or REJOIN (LoRaWAN Join Request)
	 */
	uint16_t rx_jreq_cnt;

	/*
	 * @brief Rx counter of messages for networks described into the ExpConfig
	 */
	uint16_t rx_network_cnt;
};

typedef struct SosMessagePayload SosMessagePayload_t;


/**
 * @brief Diagnosis Message Payload
 * Transport: Data Frame Up
 */
struct __attribute__((__packed__)) DiagMessagePayload
{
    /*
     * @brief Firmware version
     */
    uint32_t  firmware_version;

    /*
     * @brief Slot ID
     */
    uint8_t  slot_id; // 0 or 1

    /*
     * @brief Board uptime (in seconds)
     */
    uint32_t  uptime : 24;

    /*
     * @brief Temperature
     */
    uint8_t  temperature;

    /*
     * @brief RTC timestamp
     */
    uint32_t timestamp;

    /*
     * @brief Seconds before poweroff
     */
    uint16_t seconds_before_poweroff;

    /*
     * @brief latitude in °
     * @TODO int24_t instead of float
     */
    float  latitude;

    /*
     * @brief longitude in °
     * @TODO int24_t instead of float
     */
    float  longitude;

    /*
     * @brief altitude (in Km)
     * @TODO uint24_t (in meter) instead of float
     */
    float  altitude;

    /*
     * @brief Orientation X
     * @TODO int16_t instead of float
     */
    float  x;

    /*
     * @brief Orientation Y
     * @TODO int16_t instead of float
     */
    float  y;

    /*
     * @brief Orientation Z
     * @TODO int16_t instead of float
     */
    float  z;

    /*
     * @brief Orientation W
     * @TODO int16_t instead of float
     */
    float  w;
};

typedef struct DiagMessagePayload DiagMessagePayload_t;

/**
 * @brief Telemetry Message Payload
 * Transport: Data Frame Up
 */
struct __attribute__((__packed__)) TelemetryMessagePayload
{
    /*
     * @brief Experiment identifier
     */
    uint16_t experiment_id;

    /*
     * @brief Entry identifier
     */
    uint8_t entry_id;

    /*
     * @brief RF power
     */
    uint8_t rfpower;

    /*
     * @brief RTC timestamp
     */
    uint32_t timestamp;

    /*
     * @brief Seconds before poweroff
     */
    uint16_t seconds_before_poweroff;

    /*
     * @brief temperature (in ??)
     */
    uint8_t temperature;

    /*
     * @brief latitude in °C
     * @TODO uint24_t instead of float
     */
    float  latitude;

    /*
     * @brief longitude in °C
     * @TODO uint24_t instead of float
     */
    float  longitude;

    /*
     * @brief altitude (in Km)
     * @TODO uint24_t (in meter) instead of float
     */
    float  altitude;

    /*
     * @brief Orientation X
     * @TODO int16_t instead of float
     */
    float  x;

    /*
     * @brief Orientation Y
     * @TODO int16_t instead of float
     */
    float  y;

    /*
     * @brief Orientation Z
     * @TODO int16_t instead of float
     */
    float  z;

    /*
     * @brief Orientation W
     * @TODO int16_t instead of float
     */
    float  w;

    /*
     * @brief Roll (X) angular rate in mdps (from gyroscope)
     */
    int32_t  roll;

    /*
     * @brief Pitch (Y) angular rate in mdps (from gyroscope)
     * @TODO change for int16_t
     */
    int32_t  pitch;

    /*
     * @brief Yaw (Z) angular rate in mdps (from gyroscope)
     * @TODO change for int16_t
     */
    int32_t  yaw;
};

typedef struct TelemetryMessagePayload TelemetryMessagePayload_t;



/**
 * @brief Repeated Message Payload
 * Transport: Data Frame Up
 */
struct __attribute__((__packed__)) RepeatedMessagePayload
{
    /*
     * @brief Experiment identifier
     */
    uint16_t experiment_id;

    /*
     * @brief Entry identifier
     */
    uint8_t entry_id;

    /*
     * @brief LoRaWAN datarate
     */
    uint8_t dr;

    /*
     * @brief ESP
     */
    uint8_t esp;

    /*
     * @brief RF power for retransmission
     */
    uint8_t rfpower;

    /*
     * @brief frame bytes
     */
    uint8_t frame[REPEATED_FRAME_MAX_LEN];

};

typedef struct RepeatedMessagePayload RepeatedMessagePayload_t;


/**
 * @brief App Clock Message Payload
 * Transport: Data Frame Down (MIC with EC77)
 *
 * Proposition for an extension of LoRaWAN Application Layer Clock Synchronization v1.0.0 Specification
 * for broadcasting securely time to isolated ground endpoints from a cubesat
 * @see https://lora-alliance.org/resource-hub/lorawanr-application-layer-clock-synchronization-specification-v100
 *
 * LoRaWAN® Application Layer Clock Synchronization Specification, authored by the FUOTA Working Group of the
 * LoRa Alliance® Technical Committee, proposes an application layer messaging package running over LoRaWAN®
 * to synchronize the real-time clock of an end-device to the network’s GPS clock with second accuracy.
 *
 * This package is useful for end-devices which do not have access to other accurate time source.
 * An end-device using LoRaWAN 1.1 or above SHOULD use DeviceTimeReq MAC command instead of this package.
 * ClassB end-devices have a more efficient way of synchronizing their clock, the classB network beacon. They
 * SHOULD NOT use this package and directly use the beacon time information.
 * End-devices with an accurate external clock source (e.g.: GPS) SHOULD use that clock source instead.
 *
 * Remark: Since GPS clock sources can be jammed or spoofed, this package can be used for secure time distribution.
 *         https://wiki.eclipse.org/images/3/3a/Eclipse-IoTDay2020Grenoble-friedt.pdf
 */
struct __attribute__((__packed__)) AppClockMessagePayload
{
	// First command X_APP_CLOCK_CID_PackageVersionInfo

	uint8_t cid0; // Command ID. always equals to X_APP_CLOCK_CID_PackageVersionInfo
	/**
	 * PackageIdentifier uniquely identifies the package. For the “clock synchronization package”
	 * this identifier is 1.
	 */
	uint8_t package_identifier;
	/**
	 * PackageVersion corresponds to the version of the package specification implemented by the
	 * cubesat
	 */
	uint8_t package_version;

	// Second command X_APP_CLOCK_CID_AppTimeSetReq
	uint8_t cid1; // Command ID. always equals to X_APP_CLOCK_CID_AppTimeSetReq

	uint8_t param_rfu: 6;

	/**
	 * The precision is interpreted as a base-ten exponent of the message’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	/**
	 *  TimeToSet is the time for setting the end-device clock and is expressed as the time
	 * since 180 00:00:00, Sunday 6th of January 1980 (start of the GPS epoch) modulo 2^32.
	 */
	uint32_t time_to_set;

	/**
	 * Cubesat position
	 * @see Class B(eacon) specification : 13.3.1 Gateway GPS coordinate: InfoDesc=0, 1 or 2
	 */

	uint8_t cid2; // Command ID. always equals to X_APP_CLOCK_CID_NoradInfoReq
	uint32_t noradnumber : 24; // NORAD Catalog Number of the satellite https://celestrak.com/satcat (51087 for STORK-1)
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint32_t alt : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint8_t loc_param_source: 1; // 1 for GNSS, 0 for TLE+RTC
	uint8_t loc_param_prec: 7;
};

typedef struct AppClockMessagePayload AppClockMessagePayload_t;


/**
 * @brief TLE Message Payload
 * Transport: Data Frame Down (MIC with AES or ECC)
 *
 * For re-synchronized TLE parameters on the ground endpoint (for computed visibility windows)
 * @TODO check completude
 * @TODO check precision lost with int16_t and uint16_t instead of float
 */
struct __attribute__((__packed__)) TleMessagePayload
{
	uint8_t time_param_rfu: 5;
	/**
	 * the precision is interpreted as a base-ten exponent of the message’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * time_param_prec == 5 for second precision
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t time_param_prec: 3;
	/**
	 *  TimeToSet is the time for setting the end-device clock and is expressed as the time
	 * since 180 00:00:00, Sunday 6th of January 1980 (start of the GPS epoch) modulo 2^32.
	 */
	uint32_t time_to_set;

	// TLE parameters

  	uint32_t   							noradnumber : 24;		// NORAD Satellite calatog number (see https://celestrak.com/satcat/search-results.php) (51087 for STORK-1)
	uint16_t   							YE;		// Epoch Year    			year
	uint16_t /* instead of float */ 	TE;		// Epoch time    			days
	int16_t /* instead of float */ 		IN;		// Inclination   			deg * ????
	int16_t /* instead of float */ 		RA;		// R.A.A.N.      			deg * ????
	int16_t /* instead of float */ 		EC;		// Eccentricity  			 -
	int16_t /* instead of float */ 		WP;		// Arg perigee   			deg * ????
	int16_t /* instead of float */ 		MA;		// Mean anomaly  			deg * ????
	int16_t /* instead of float */ 		MM;		// Mean motion   			rev/d
	int16_t /* instead of float */ 		M2;		// Decay Rate    			rev/d/d
	int16_t /* instead of float */ 		RV;		// Orbit number  			 -
	int16_t /* instead of float */ 		ALON;	// Sat attitude				deg * ????
	int16_t /* instead of float */ 		ALAT;	// Sat attitude				deg * ????
    uint16_t   							DE;		// Epoch Fraction of day

	uint8_t cubesat_antenna_aperture;			// antenna aperture			deg

	// TODO add cubesat signature on TleMessage
	// uint8_t signature[CRYPTO_SIGN_ED25519_SIGNATURE_BYTES];

};

typedef struct TleMessagePayload TleMessagePayload_t;


/**
 * @brief ED25519 public key size
 */
#define CRYPTO_SIGN_ED25519_PUBLICKEYBYTES         32U

/**
 * @brief ED25519 Public Key Message Payload (for updating the endpoints registries)
 * Transport: Data Down Frame
 *
 * The DevAddr contains the NetId of the (official) network
 * The MIC SHALL be computed with the ECC private key of satellite (self-signed) or with a master key.
 */
struct __attribute__((__packed__)) ED25519PubKeyMessagePayload
{
	uint32_t noradnumber : 24; // NORAD Catalog Number of the satellite https://celestrak.com/satcat (51087 for STORK-1)
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint32_t alt : 24; // The altitude (in meter)
	uint8_t pubkey[CRYPTO_SIGN_ED25519_PUBLICKEYBYTES]; // 32 bytes
};

typedef struct ED25519PubKeyMessagePayload ED25519PubKeyMessagePayload_t;


/**
 * @brief Forwarding Message Payload
 * The payload size should not be excced 51 bytes
 * Transport: Data Frame Dn
 */
struct __attribute__((__packed__)) ForwardingMessagePayload
{
	uint16_t forwarding_timestamp;

	/*
	 * @brief frame to forward
	 */
	uint8_t frame_to_forward[51-sizeof(uint16_t)];
};

typedef struct ForwardingMessagePayload ForwardingMessagePayload_t;


typedef enum  { STOP_EXP, PAUSE_TX, RESUME_TX, DIAG, POWEROFF, REBOOT } ACTION_TYPE;

/**
 * @brief Action Message Payload
 * Action to be executed by the paylaod
 * Transport: Data Frame Down (send by the ground segment
 * The payload size should not be exceed 51 bytes
 */
struct __attribute__((__packed__)) ActionMessagePayload
{
    /*
     * @brief Action operation ID (STOP_TX, RESUME_TX, REBOOT ...)
     */
	ACTION_TYPE	action;
};

typedef struct ActionMessagePayload ActionMessagePayload_t;


/**
 * @brief Time Test Message Payload
 * Message for testing time distribution
 * Transport: Data Frame Down (send by the ground segment
 * The payload size should not be exceed 51 bytes
 */
struct __attribute__((__packed__)) TimingTestMessagePayload
{
	uint8_t		number;
	uint32_t	interval_in_msec; // use to compute the next timestamp of the 'tx_mode' parameter = TIMESTAMPED
};

typedef struct TimingTestMessagePayload TimingTestMessagePayload_t;



uint8_t getESPU8(float rssi, float snr);

float getESP(uint8_t espu8);

void UpTextMessagePayload_printf(const UpTextMessagePayload_t* p, const uint8_t payload_size);

void ForwardingMessagePayload_printf(const ForwardingMessagePayload_t* p, const uint8_t payload_size);

void ActionMessagePayload_printf(const ActionMessagePayload_t* p);

void TimingTestMessagePayload_printf(const TimingTestMessagePayload_t* p);

void MessagePayloadSizeof_printf(void);

#endif
