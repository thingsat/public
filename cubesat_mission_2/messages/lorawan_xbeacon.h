/*
 * Copyright (C) 2020-2023 Université Grenoble Alpes
 */

/*
 * Author : Didier Donsez, Université Grenoble Alpes
 */

/*
 * Thingsat Mission Scenarii :: Data structures
 */

/*
 * Types for the format of the extended beacons send by the Thingsat payload
 */


#ifndef LORAWAN_XBEACON_H
#define LORAWAN_XBEACON_H

#include <stdint.h>
#include <stdbool.h>

#ifdef MODULE_C25519
#include "edsign.h"
#else
#define EDSIGN_SIGNATURE_SIZE		(64U)
#endif

/**
 * @brief Extended Beacon SF12 Message Payload (signed with ED25519 keys)
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The ED25519 signature is 64-bytes long
 */
struct __attribute__((__packed__)) XBeaconSF12MessagePayload
{
	uint8_t rfu1[4];
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint32_t alt : 24; // The altitude in meter
	uint32_t noradnumber : 24; // NORAD Catalog Number of the satellite https://celestrak.com/satcat (51087 for STORK-1)
	uint8_t signature[EDSIGN_SIGNATURE_SIZE];
};

typedef struct XBeaconSF12MessagePayload XBeaconSF12MessagePayload_t;

/**
 * @brief Extended Beacon SF11 Message Payload (signed with ED25519 keys)
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The ED25519 signature is 64-bytes long
 */
struct __attribute__((__packed__)) XBeaconSF11MessagePayload
{
	uint8_t rfu1[3];
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint32_t alt : 24; // The altitude in meter
	uint32_t noradnumber : 24; // NORAD Catalog Number of the satellite https://celestrak.com/satcat (51087 for STORK-1)
	uint8_t signature[EDSIGN_SIGNATURE_SIZE];
};

typedef struct XBeaconSF11MessagePayload XBeaconSF11MessagePayload_t;

/**
 * @brief Extended Beacon SF10 Message Payload (signed with ED25519 keys)
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The ED25519 signature is 64-bytes long
 */
struct __attribute__((__packed__)) XBeaconSF10MessagePayload
{
	uint8_t rfu1[2];
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint32_t alt : 24; // The altitude in meter
	uint32_t noradnumber : 24; // NORAD Catalog Number of the satellite https://celestrak.com/satcat (51087 for STORK-1)
	uint8_t signature[EDSIGN_SIGNATURE_SIZE];
};

typedef struct XBeaconSF10MessagePayload XBeaconSF10MessagePayload_t;


/**
 * @brief Extended Beacon SF9 Message Payload (signed with ED25519 keys)
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The ED25519 signature is 64-bytes long
 */
struct __attribute__((__packed__)) XBeaconSF9MessagePayload
{
	uint8_t rfu1[1];
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint32_t alt : 24; // The altitude in meter
	uint32_t noradnumber : 24; // NORAD Catalog Number of the satellite https://celestrak.com/satcat (51087 for STORK-1)
	uint8_t signature[EDSIGN_SIGNATURE_SIZE];
};

typedef struct XBeaconSF9MessagePayload XBeaconSF9MessagePayload_t;


/**
 * @brief Extended Beacon SF8 Message Payload (signed with ED25519 keys)
 * Transport: Beacon
 *
 * @see LoRaWAN® L2 1.0.4 Specification, Section 13.2 Beacon Frame Format
 * All beacons are transmitted in radio packet implicit mode, that is, without a LoRa physical header and with no CRC appended by the radio.
 * The beacon Preamble SHALL begin with (a longer than default) 10 unmodulated symbols
 * The ED25519 signature is 64-bytes long
 */
struct __attribute__((__packed__)) XBeaconSF8MessagePayload
{
	uint8_t param_rfu: 6;
	/**
	 * The precision is interpreted as a base-ten exponent of the beacon’s transmit time precision 10(−6+Prec) s, where the default value of Prec is 0.
	 * @see Section 13.5 Beaconing Precise Timing
	 */
	uint8_t param_prec: 2;
	uint32_t time;
	uint8_t infodesc; // = 0 For a single omnidirectional antenna gateway, the value of InfoDesc is 0 when broadcasting GPS coordinates
	uint32_t lat : 24; // The north–south latitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 90° south (the South Pole) and 223 corresponds to 90° north (the North Pole).  Note: It is not possible to describe 90° north because 2^23-1 is the largest number that can be represented in two’s complement notation. This approximately 1.2 m position error at the North Pole is considered small.
	uint32_t lng : 24; // The east–west longitude SHALL be encoded using a two’s complement 24-bit word, where −223 corresponds to 180° west and 223 corresponds to 180° east.
	uint32_t alt : 24; // The altitude in meter
	uint32_t noradnumber : 24; // NORAD Catalog Number of the satellite https://celestrak.com/satcat (51087 for STORK-1)
	uint8_t signature[EDSIGN_SIGNATURE_SIZE];
};

typedef struct XBeaconSF8MessagePayload XBeaconSF8MessagePayload_t;


#endif
