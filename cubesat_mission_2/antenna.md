# Thingsat @ SOWA antenna

Designers: Nhu-Huan NGuyen and Tan-Phu Vuong, [CROMA](https://croma.grenoble-inp.fr)

The patch antenna enables communications on 863-870 MHz band ([EU868](https://resources.lora-alliance.org/technical-specifications/rp002-1-0-4-regional-parameters)).

The polarization is circular.

The substrat is [Roger](https://rogerscorp.com/) [RO4360](https://www.rogerscorp.com/-/media/project/rogerscorp/documents/advanced-electronics-solutions/english/data-sheets/ro4360g2-high-frequency-laminates-data-sheet.pdf).

### Performance

+ Gain: 3.34 dBi
+ Half-power beamwidth (up tu -3 dB) ~ 70 deg Polarization
+ Circular: yes
+ Band: 867.5 MHz - 869.525 MHz : Yes
+ Dimension: 100mm x 100 mm x 1.524 mm.
+ Estimated weight: ~38 g (dielectric + copper) without SMP connector (1.5 g).

## Diagrams

![Antenna Diag](./media/antenna-diag-00.png)
![Antenna Diag](./media/antenna-diag.png)
![Antenna Diag](./media/antenna-diag-01.png)
![Antenna Diag](./media/antenna-diag-02.png)
![Antenna Diag](./media/antenna-diag-03.png)
![Antenna Diag](./media/antenna-diag-04.png)
![Antenna Diag](./media/antenna-diag-05.png)

## Flight model of the antenna

![Antenna 6U cubesat](./media/antenna-fm-01.jpg)
![Antenna 6U cubesat](./media/antenna-fm-02.jpg)
![Antenna on SOWA_2](./media/sowa+bg-small.jpg )

Created with SIMULIA CST Studio Suite.

