/*
 * Copyright (C) 2022-2023 Université Grenoble Alpes
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 *
 * Author: Didier Donsez, Université Grenoble Alpes
 */
 
syntax = "proto3";
import "nanopb.proto";

package thingsat.v1;


// TODO message definition for Position, Orientation, Temperature, Magnetometer, Accelerometer, Gyrometer, PowerConsumption

/**
 * @brief Environment
 */
message Environment
{
	// ==========================================================
	// Timestamps
	// ==========================================================

	uint32 uptime = 1; //! Payload uptime (since last reboot)

	fixed32 timestamp_before_sync = 2; //!< Timestamp (as a standard 32-bit unix timestamp) before the UpApiPacket_TimestampResponse

	fixed32 timestamp = 3; //!< Timestamp as a standard 32-bit unix timestamp

	uint32 seconds_before_poweroff = 4; //! Seconds before power off

	//! For position returned from the GPS device (~5m accuracy).

	float gps_lat = 5;
	float gps_lng = 6;
	float gps_alt = 7;

	// ==========================================================
	// Position
	// ==========================================================
	//! For position calculated from TLE (~100m accuracy),
	//! Getting the position from TLE is instantaneous.
	//! Optional since it can be calculed from timestamp and current TLE

	optional float tle_lat = 8;
	optional float tle_lng = 9;
	optional float tle_alt = 10;

	// ==========================================================
	// Orientation
	// ==========================================================
	//! Orientation is returned as ECI coordinates
	//! @see https://en.wikipedia.org/wiki/Earth-centered_inertial
	//! @see http://www.tu-berlin.de/fileadmin/fg169/miscellaneous/Quaternions.pdf
	//! For space saving communication bandwidth, an optimization with float16_t (half-precision (16-bit) floating point) is planned for future version : float16_t will be encoded as uint32

	/// `w` component of the ECI quaternion
	float w = 11;
	/// `x` component of the ECI quaternion
	float x = 12;
	/// `y` component of the ECI quaternion
	float y = 13;
	/// `z` component of the ECI quaternion
	float z = 14;

	// ==========================================================
	// Temperature (4x STTS751)
	// ==========================================================
	// !< current temperature in °C or MAX_FLOAT if invalid
	//! For space saving communication bandwidth, an optimization with float16_t (half-precision (16-bit) floating point) is planned for future version : float16_t will be encoded as uint32
	repeated float temperature_current = 15 [(nanopb).max_size = 4, (nanopb).fixed_length = true];
	repeated float temperature_max = 16 [(nanopb).max_size = 4, (nanopb).fixed_length = true];
	repeated float temperature_min = 17 [(nanopb).max_size = 4, (nanopb).fixed_length = true];
	repeated float temperature_avg = 18 [(nanopb).max_size = 4, (nanopb).fixed_length = true];

	// ==========================================================
	// Magnetometer
	// ==========================================================
	//! For space saving communication bandwidth, an optimization with float16_t (half-precision (16-bit) floating point) is planned for future version : float16_t will be encoded as uint32
	// TODO current, max, min, avg

	// ==========================================================
	// Accelerometer 
	// ==========================================================
	//! For space saving communication bandwidth, an optimization with float16_t (half-precision (16-bit) floating point) is planned for future version : float16_t will be encoded as uint32
	// TODO current, max, min, avg

	// ==========================================================
	// Gyrometer 
	// ==========================================================
	//! For space saving communication bandwidth, an optimization with float16_t (half-precision (16-bit) floating point) is planned for future version : float16_t will be encoded as uint32
	// TODO current, max, min, avg

	// ==========================================================
	// Power consumption
	// ==========================================================
	//! For space saving communication bandwidth, an optimization with float16_t (half-precision (16-bit) floating point) is planned for future version : float16_t will be encoded as uint32
	// TODO current, max, min, avg
};
