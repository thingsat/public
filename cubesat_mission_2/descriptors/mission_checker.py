#!/usr/bin/env python3
#
# Copyright (C) 2023 Université Grenoble Alpes
#
# Author: Didier Donsez, Université Grenoble Alpes
#
# Check the consistency of the experiment parameters
#

import experiment_pb2
#import tle_pb2
#import geofences_pb2
import sys
import time
from datetime import datetime

if len(sys.argv) != 2:
    print("Usage:", sys.argv[0], "experiment.protobuf")
    print("Usage:", sys.argv[0], "experiment.protobuf tle.protobuf")
    print("Usage:", sys.argv[0], "experiment.protobuf tle.protobuf geofences.protobuf")
    sys.exit(-1)

e = experiment_pb2.Experiment()
#tle =tle_pb2.TLE()
#geofences = geofences_pb2.Geofences()

# Read the existing experiment.
with open(sys.argv[1], "rb") as f:
    e.ParseFromString(f.read())

print(e)

error_counter = 0
warning_counter = 0
last_endtime = 0
experiment_duration = 0

frames_signed_by_ed25519 = [
    experiment_pb2.MISSION_PAYLOAD_TYPE.PAYLOAD_TYPE_XBEACON,
    experiment_pb2.MISSION_PAYLOAD_TYPE.PAYLOAD_TYPE_ED25519_TLE,
    experiment_pb2.MISSION_PAYLOAD_TYPE.PAYLOAD_TYPE_ED25519_APP_CLOCK,
    experiment_pb2.MISSION_PAYLOAD_TYPE.PAYLOAD_TYPE_ED25519_PUBKEY
]

def CheckBaseTime(experiment):
    epoch_time = int(time.time())
    if experiment.base_time != 0:
        if experiment.base_time < epoch_time:
            print(f'ERROR: base_time should be in the future')
            error_counter += 1
        else:
            print(f'INFO: base_time in {experiment.base_time - epoch_time} seconds')


def CheckLen(experiment):

    if len(e.config) > 0:
        print(f'INFO: config number        : {len(e.config)}')
    else:
        print(f'ERROR: config can not be empty')
        error_counter += 1

    if len(e.networks) > 0:
        print(f'INFO: networks number      : {len(e.networks)}')
    else:
        print(f'ERROR: networks can not be empty')
        error_counter += 1

    if len(e.endpoints) > 0:
        print(f'INFO: endpoints number     : {len(e.endpoints)}')
    else:
        print(f'ERROR: endpoints can not be empty')
        error_counter += 1

    if len(e.txParameters) > 0:
        print(f'INFO: txParameters number  : {len(e.txParameters)}')
    else:
        print(f'ERROR: txParameters can not be empty')
        error_counter += 1

    if len(e.entries) > 0:
        print(f'INFO: entries number       : {len(e.entries)}')
    else:
        print(f'ERROR: entries can not be empty')
        error_counter += 1


def CheckTxParametersFrequenciesLen(txParameters_idx, frequencies):
    if len(frequencies) == 0:
        print(
            f'ERROR: txParameters: {txParameters_idx} frequencies can not be empty')
        error_counter += 1


def CheckEndpointIdx(entry_idx, endpoint_idx):
    if endpoint_idx > len(e.endpoints):
        print(f'ERROR: Entry: {entry_idx} endpoint_idx overflow')
        error_counter += 1


def CheckRx(entry_idx, rx):
    print(f'INFO: Entry: {entry_idx} RX: {rx.networks_mask}')


def CheckTx(entry_idx, tx):
    CheckEndpointIdx(entry_idx, tx.endpoint_idx)
    if len(tx.payload_types) == 0:
        print(f'ERROR: Entry: {entry_idx} TX : payload_types can not be empty')
        error_counter += 1
    if len(tx.payload_sizes) == 0:
        global warning_counter
        print(f'WARNING: Entry: {entry_idx} TX : payload_sizes is empty')
        warning_counter += 1
    if tx.HasField('lbt_timeout') and tx.lbt_timeout == 0:
        print(f'WARNING: Entry: {entry_idx} TX : lbt_timeout == 0')
        warning_counter += 1
    if tx.HasField('lbt_timeout') and not tx.HasField('lbt_threshold'):
        print(
            f'WARNING: Entry: {entry_idx} TX : default value for lbt_threshold since lbt_timeout is set')
        warning_counter += 1
    if not e.HasField('ed25519_sat'):
        for pt in tx.payload_types:
            if pt in frames_signed_by_ed25519:
                print(
                    f'ERROR: Entry: {entry_idx} {pt} requires ed25519_sat field')
                error_counter += 1


def CheckRepeat(entry_idx, repeat):
    if repeat.endpoint_idx != 0xFF:
        CheckEndpointIdx(entry_idx, repeat.endpoint_idx)


def CheckStore(entry_idx, store):
    print(f'INFO: Entry: {entry_idx} STORE :  {store.networks_mask}')


def CheckForward(entry_idx, forward):
    global error_counter
    if forward.endpoint_idx != 0xFF:
        CheckEndpointIdx(entry_idx, forward.endpoint_idx)
    if forward.queue_after_load and forward.delete_after_load:
        print(
            f'INFO: Entry: {entry_idx} FORWARD :  can not have both queue_after_load==True and delete_after_load==True')
        error_counter += 1
    if e.experiment_id != forward.store_experiment_id:
        print(f'WARNING: Entry: {entry_idx} FORWARD : please, check if {forward.store_experiment_id} {forward.store_entry_id} is a STORE entry whithout queueing')
        warning_counter += 1
    if entry_idx <= forward.store_entry_id:
        print(f'ERROR: Entry: {entry_idx} FORWARD store_entry_id={forward.store_entry_id} is after this FORWARD entry')
        error_counter += 1
        es = e.entries[forward.store_entry_id]
        if not es.HasField('store'):
            print(f'ERROR: Entry: {entry_idx} FORWARD store_entry_id={forward.store_entry_id} is not a STORE entry')
            error_counter += 1
        else:
            if es.store.queueing:
                print(f'ERROR: Entry: {entry_idx} FORWARD store_entry_id={forward.store_entry_id} shoud not queueing==true')
                error_counter += 1

def CheckSpectralScan(entry_idx, spectral_scan):
    print(f'INFO: Entry: {entry_idx} SPECTRAL_SCAN : ')


def CheckRanging(entry_idx, ranging):
    CheckEndpointIdx(entry_idx, ranging.endpoint_idx)


def CheckTinyml(entry_idx, tinyml):
    CheckEndpointIdx(entry_idx, tinyml.endpoint_idx)
    if len(tinyml.models) == 0:
        print(f'ERROR: Entry: {entry_idx} TINYML : models can not be empty')


def CheckStartTime(entry_idx, entry):
    global last_endtime
    global experiment_duration

    if entry.start_time < last_endtime:
        print(f'ERROR: entry {entry_idx} starts before {last_endtime}')
        error_counter += 1

    experiment_duration += entry.end_time
    last_endtime = entry.start_time + entry.end_time


def CheckArea(entry_idx, entry, base_time, tle, whitelist_areas):
    print(f'INFO: Entry: {entry_idx} check area')


def CheckEntries(experiment):

    entry_idx = 0
    for entry in experiment.entries:

        CheckStartTime(entry_idx, entry)

        # TODO CheckArea(entry_idx, entry, base_time, tle, whitelist_areas)

        if entry.HasField('rx'):
            CheckRx(entry_idx, entry.rx)
        elif entry.HasField('tx'):
            CheckTx(entry_idx, entry.tx)
        elif entry.HasField('repeat'):
            CheckRepeat(entry_idx, entry.repeat)
        elif entry.HasField('store'):
            CheckStore(entry_idx, entry.store)
        elif entry.HasField('forward'):
            CheckForward(entry_idx, entry.forward)
        elif entry.HasField('spectral_scan'):
            CheckSpectralScan(entry_idx, entry.spectral_scan)
        elif entry.HasField('ranging'):
            CheckRanging(entry_idx, entry.ranging)
        elif entry.HasField('tinyml'):
            CheckTinyml(entry_idx, entry.tinyml)

        entry_idx += 1
    return 0

# TODO Check base_time according now : should be in the future !

#from_now = datetime.now() - datetime(base_time)
#print(str(from_now))

CheckLen(e)
# TODO CheckTxParametersFrequenciesLen(txParameters_idx, frequencies)
CheckEntries(e)

# If TLE (tle.protobuf) is provided, give GPS positions (start,end) for each entry
# Moreover, if geofences (geofence.protobuf) are provided, warn if the entry is outside a geofence.

print()

print(f'INFO: experiment ends after {last_endtime} seconds')
# TODO print the date of the end if base_time > 0
print(f'INFO: total duration of {len(e.entries)} entries is {experiment_duration} seconds')

print()

if error_counter > 0:
    print(
        f'ERROR: check completed with {error_counter} errors and  {warning_counter} warnings')
elif warning_counter > 0:
    print(
        f'INFO: check completed without error but with {warning_counter} warnings')
else:
    print(f'INFO: check completed without error or warning')