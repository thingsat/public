# Thingsat description files

Description files are encoded using [Protocol Buffer aka `protobuf`](https://protobuf.dev/) (and the [`nanopb`](https://github.com/nanopb/nanopb) plugin).

## Experiment file

The experiment description is described in [`experiment.proto`](./experiment.proto).

The encoded representation of an experiment is stored in a file with the `.EXP` extension.

The file [`exp_sample.text`](./exp_sample.text) is an example of experiment description (`text` format). It can be edited and encoded using `protoc` (see the following command lines).

```bash
export RIOTBASE=~/github/RIOT-OS/RIOT
protoc --encode=thingsat.v1.Experiment \
	--plugin=$RIOTBASE/build/pkg/nanopb/generator/protoc-gen-nanopb \
	-I $RIOTBASE/build/pkg/nanopb/generator/proto:. \
	experiment.proto \
	< exp_sample.text > 0001.EXP
wc -c  0001.EXP
hexdump 0001.EXP
```

## Diagnosis file

The encoded representation of a diagnosis is stored in a file with the `.DIA` extension.

TBC

## Result file

The encoded representation of an experiment entry result is stored in a file with the `.RES` extension.

TBC

## Packet file

The encoded representation of a set of packets (and metadata) received by the gateway is stored in a file with the `.PKT` extension.

TBC
