# EWSS (Emergency Warning Satellite Service) and SAR (Search-and-Rescue)

Galileo is a satellite positioning system (radio navigation) developed by the European Union (EU) that has been partially operational since late 2016. Most GNSS modules embedded in smartphones and mobile connected devices (watches, trackers, etc.) receive signals from the Galileo satellite constellation, as well as from other constellations: GPS (USA), GLONASS (Russia), BEIDOU (China), and QZSS (Japan).

Galileo provides additional services, such as alert services (called “Emergency Warning Satellite Services (EWSS)”) and rescue services (called “Search and Rescue (SAR)”).

Mission scenarii of Thingsat include:
* EWSS-compliant messages broadcasting over LoRaWAN (LoRa and LR-FHSS)
* SAR-compliant messages relaying (as well as store-carry-and-forward) over LoRaWAN (LoRa and LR-FHSS)

EWSS-compliant messages are optionally signed using ED25519 signatures.

SAR-compliant messages are optionally signed using AES256 (CMAC) signatures.

* https://www.gsc-europa.eu/sites/default/files/sites/all/files/EWSS-CAMF_v1.0.pdf
* https://www.unoosa.org/documents/pdf/icg/2023/ICG-17/icg17_wgb_20.pdf
* https://www.gsc-europa.eu/galileo/services/search-and-rescue-sar-galileo-service
* https://www.gsc-europa.eu/sites/default/files/sites/all/files/Galileo-SAR-SDD.pdf

![ewss](media/ewss-01.jpg)
![sar](media/search-and-rescue.jpg)
