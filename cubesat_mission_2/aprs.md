# Automatic Packet Reporting System (APRS)

Automatic Packet Reporting System (APRS) is an amateur radio-based system for real time digital communications of information of immediate value in the local area. Data can include object Global Positioning System (GPS) coordinates, non-directional beacon, weather station telemetry, text messages, announcements, queries, and other telemetry.

Mission scenarii of Thingsat include APRS-compliant messages relaying (as well as store-carry-and-forward) over LoRaWAN (LoRa and LR-FHSS)

APRS-compliant LoRa messages are optionally signed using ED25519 signatures.

APRS-compliant LoRaWAN messages are signed using AES128 (CMAC) signatures. `AppSKey` is `cafebabe12345678cafebabe12345678`. APRS-compliant LoRaWAN messages can be authenticated by the home LNS and by the visitor LNS (thru roaming agreement).

https://en.wikipedia.org/wiki/Automatic_Packet_Reporting_System