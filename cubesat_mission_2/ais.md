# AIS (Automatic Identification System)

The automatic identification system (AIS) is an automatic tracking system that uses transceivers on ships and is used by vessel traffic services (VTS). When satellites are used to receive AIS signatures, the term Satellite-AIS (S-AIS) is used.

Mission scenarii of Thingsat include AIS-compliant messages relaying (as well as store-carry-and-forward) over LoRaWAN (LoRa and LR-FHSS)

AIS-compliant messages are optionally signed using ED25519 signatures.

AIS-compliant messages are optionally signed using AES256 (CMAC) signatures.

* https://en.wikipedia.org/wiki/Automatic_identification_system